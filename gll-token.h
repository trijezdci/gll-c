/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-token.h
 *
 * Public interface for GLL token module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_TOKEN_H
#define GLL_TOKEN_H

#include "gll-common.h"

#include <stdbool.h>


/* --------------------------------------------------------------------------
 * type gll_token_t
 * --------------------------------------------------------------------------
 * Enumerated token values representing EBNF terminal symbols.
 * ----------------------------------------------------------------------- */

typedef enum {
  /* Null Token */
  
  TOKEN_UNKNOWN,
  
  /* Reserved Words */

  TOKEN_ALIAS,            /* "alias" */
  TOKEN_EOF,              /* "EOF" */
  TOKEN_ENDG,             /* "endg" */
  TOKEN_GRAMMAR,          /* "grammar" */
  TOKEN_RESERVED,         /* "reserved" */
  
  /* Identifiers */
  
  TOKEN_LOWER_IDENT,      /* LowerLetter ('_' | Letter | Digit)* */
  TOKEN_UPPER_IDENT,      /* UpperLetter ('_' | Letter | Digit)* */
  
  /* Literals */
  
  TOKEN_STRING,           /* '"' Character* '"' | "'" Character* "'" */
  TOKEN_QUOTED_LOWER,     /* '"' 'a' .. 'z' '"' | "'" 'a' .. 'z' "'" */
  TOKEN_QUOTED_UPPER,     /* '"' 'A' .. 'Z' '"' | "'" 'A' .. 'Z' "'" */
  TOKEN_QUOTED_DIGIT,     /* '"' '0' .. '9' '"' | "'" '0' .. '9' "'" */
  TOKEN_QUOTED_NONALNUM,  /* non-alphanumeric quoted printable */
  TOKEN_CHAR_CODE,        /* '0u' ( '0' .. '9' | 'A' .. 'F' )+ */
  
  TOKEN_MALFORMED_LITERAL,
  
  /* Special Symbols */
  
  TOKEN_ASTERISK,         /* '*' */
  TOKEN_PLUS,             /* '+' */
  TOKEN_COMMA,            /* ',' */
  TOKEN_PERIOD,           /* '.' */
  TOKEN_DOTDOT,           /* '..' */
  TOKEN_COLONEQUAL,       /* ':=' */
  TOKEN_SEMICOLON,        /* ';' */
  TOKEN_EQUAL,            /* '=' */
  TOKEN_QMARK,            /* '?' */
  TOKEN_BAR,              /* '|' */
  TOKEN_LEFT_PAREN,       /* '(' */
  TOKEN_RIGHT_PAREN,      /* ')' */
  TOKEN_END_OF_FILE,
  
  /* Enumeration Terminator */
  
  TOKEN_END_MARK /* marks the end of this enumeration */
} gll_token_t;


/* --------------------------------------------------------------------------
 * translations
 * ----------------------------------------------------------------------- */

#define TOKEN_KLEENE_STAR TOKEN_ASTERISK
#define TOKEN_KLEENE_PLUS TOKEN_PLUS
#define TOKEN_KLEENE_OPTION TOKEN_QMARK
#define TOKEN_DEFINITION TOKEN_COLONEQUAL


/* --------------------------------------------------------------------------
 * first and last reserved word tokens
 * ----------------------------------------------------------------------- */

#define FIRST_RESERVED_WORD_TOKEN TOKEN_ALIAS
#define LAST_RESERVED_WORD_TOKEN TOKEN_RESERVED


/* --------------------------------------------------------------------------
 * first and last literal tokens
 * ----------------------------------------------------------------------- */

#define FIRST_LITERAL_TOKEN TOKEN_STRING
#define LAST_LITERAL_TOKEN TOKEN_CHAR_CODE


/* --------------------------------------------------------------------------
 * first and last special symbol tokens
 * ----------------------------------------------------------------------- */

#define FIRST_SPECIAL_SYMBOL_TOKEN TOKEN_ASTERISK
#define LAST_SPECIAL_SYMBOL_TOKEN TOKEN_RIGHT_PAREN


/* --------------------------------------------------------------------------
 * function gll_is_valid_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a terminal symbol, otherwise false.
 * ----------------------------------------------------------------------- */

inline bool gll_is_valid_token (gll_token_t token);


/* --------------------------------------------------------------------------
 * function gll_is_resword_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a reserved word, otherwise false.
 * ----------------------------------------------------------------------- */

inline bool gll_is_resword_token (gll_token_t token);


/* --------------------------------------------------------------------------
 * function gll_is_literal_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a literal, otherwise false.
 * ----------------------------------------------------------------------- */

inline bool gll_is_literal_token (gll_token_t token);


/* --------------------------------------------------------------------------
 * function gll_is_special_symbol_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a special symbol, otherwise false.
 * ----------------------------------------------------------------------- */

inline bool gll_is_special_symbol_token (gll_token_t token);


/* --------------------------------------------------------------------------
 * function gll_token_for_resword(lexeme, length)
 * --------------------------------------------------------------------------
 * Tests if the given lexeme represents a reserved word and returns the
 * corresponding token or TOKEN_UNKNOWN if it does not match a reserved word.
 * ----------------------------------------------------------------------- */

gll_token_t gll_token_for_resword (const char *lexeme, uint_t length);


/* --------------------------------------------------------------------------
 * function m2c_lexeme_for_resword(token)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to a NUL terminated character string with
 * the lexeme for the reserved word represented by token.  Returns NULL
 * if the token does not represent a reserved word.
 * ----------------------------------------------------------------------- */

const char *gll_lexeme_for_resword (gll_token_t token);


/* --------------------------------------------------------------------------
 * function gll_lexeme_for_special_symbol(token)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to a NUL terminated character string with
 * the lexeme for the special symbol represented by token.  Returns NULL
 * if the token does not represent a special symbol.
 * ----------------------------------------------------------------------- */

const char *gll_lexeme_for_special_symbol (gll_token_t token);


/* --------------------------------------------------------------------------
 * function gll_name_for_token(token)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to a NUL terminated character string with
 * a human readable name for token.  Returns NULL if token is not valid.
 * ----------------------------------------------------------------------- */

const char *gll_name_for_token (m2c_token_t token);


#endif /* GLL_TOKEN_H */

/* END OF FILE */