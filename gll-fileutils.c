/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-fileutils.h (POSIX version)
 *
 * Implementation of filesystem utility functions.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gll-fileutils.h"

#include "gll-common.h"

#include <stdlib.h>
#include <sys/stat.h>


/* --------------------------------------------------------------------------
 * function is_valid_filename(filename)
 * --------------------------------------------------------------------------
 * Returns true if filename is a valid filename, otherwise false.
 * --------------------------------------------------------------------------
 */

#define IS_FILENAME_CHAR(_ch) \
  (((_ch) == '.') || ((_ch) == '_') || \
  (((_ch) >= 'a') && ((_ch) <= 'z')) || \
  (((_ch) >= '0') && ((_ch) <= '9')) || \
  (((_ch) >= 'A') && ((_ch) <= 'Z')))
  
bool is_valid_filename (const char *filename) {
  uint_t index = 0;
  
  if (filename[0] == ASCII_NUL) {
   return false;
  } /* end if */
    
  while (IS_FILENAME_CHAR(filename[index])) {
    index++;
    if ((filename[index] == '.') && (filename[index-1] == '.')) {
      break;
    } /* end if */
  } /* end while */
  
  return (filename[index] == ASCII_NUL);
} /* end is_valid_filename */


/* --------------------------------------------------------------------------
 * function is_valid_pathname(pathname)
 * --------------------------------------------------------------------------
 * Returns true if pathname is a valid pathname, otherwise false.
 * --------------------------------------------------------------------------
 */

bool is_valid_pathname (const char *pathname) {
  uint_t index = 0;
  
  if (pathname[0] == ASCII_NUL) {
   return false;
  } /* end if */
  
  /* advance to first slash */
  if (pathname[0] == '~') {
    index = 1;
    if (pathname[1] != '/') {
      return false;
    } /* end if */
  }
  else if (pathname[0] == '.') {
    if (pathname[1] == '/') {
      index = 1;
    }
    else if (pathname[1] == '.') {
      index = 2;
      if (pathname[2] != '/') {
        return false;
      } /* end if */
    } /* end if */
  } /* end if */
  
  if (pathname[index] == '/') {
    /* advance over subdirectories */
    while (pathname[index] == '/') {
      index++;
      while (IS_FILENAME_CHAR(pathname[index])) {
        index++;
        if ((pathname[index] == '.') && (pathname[index-1] == '.')) {
          return false;
        } /* end if */
      } /* end while */
    } /* end while */
  }
  else {
    /* advance over filename */
    while (IS_FILENAME_CHAR(pathname[index])) {
      index++;
      if ((pathname[index] == '.') && (pathname[index-1] == '.')) {
        return false;
      } /* end if */
    } /* end while */
  } /* end if */
  
  return (pathname[index] == ASCII_NUL);
} /* end is_valid_pathname */


/* --------------------------------------------------------------------------
 * function file_exists(filename)
 * --------------------------------------------------------------------------
 * Returns true if the file indicated by filename exists, otherwise false.
 * --------------------------------------------------------------------------
 */

bool file_exists (const char *filename) {
  return (access(filename, F_OK) == 0);
} /* end file_exists */


/* --------------------------------------------------------------------------
 * function filesize(filename)
 * --------------------------------------------------------------------------
 * Returns the file size of a file.  Returns INVALID_FILE_SIZE on failure.
 * --------------------------------------------------------------------------
 */

int filesize (const char *filename) {
  struct stat st;
  
  if (filename != NULL) {
    stat(filename, &st);
    return st.st_size;
  }
  else {
    return INVALID_FILE_SIZE;
  } /* end if */
} /* end filesize */


/* --------------------------------------------------------------------------
 * function macro get_cwd(cwd, size)
 * --------------------------------------------------------------------------
 * Obtains current directory path in cwd and returns a pointer to it.
 * --------------------------------------------------------------------------
 */

char *get_cwd(char *cwd, size_t size) {
  return getcwd(cwd, size);
} /* end get_cwd */

/* END OF FILE */