/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-parser.h
 *
 * Public interface for GLL parser module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_PARSER_H
#define GLL_PARSER_H

/* temporary dummy type */
typedef int *gll_ast_t; /* TO DO: AST module */


/* --------------------------------------------------------------------------
 * type gll_parser_status_t
 * --------------------------------------------------------------------------
 * Status codes for operations on type gll_parser_t.
 * --------------------------------------------------------------------------
 */

typedef enum {
  GLL_PARSER_STATUS_SUCCESS,
  GLL_PARSER_STATUS_INVALID_REFERENCE,
  GLL_PARSER_STATUS_ALLOCATION_FAILED,
  /* TO DO : add status codes for all error scenarios */
} gll_parser_status_t;


/* --------------------------------------------------------------------------
 * function gll_syntax_check(filename, status)
 * --------------------------------------------------------------------------
 * Syntax checks a grammar specification .gll file represented by filename.
 * ----------------------------------------------------------------------- */

void gll_syntax_check
  (gll_string_t filename, gll_parser_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_parse(filename, status)
 * --------------------------------------------------------------------------
 * Parses a grammar specification .gll file and returns an AST.
 * ----------------------------------------------------------------------- */

gll_ast_t gll_parse_def
  (gll_string_t filename, gll_parser_status_t *status);


#endif /* GLL_PARSER_H */

/* END OF FILE */