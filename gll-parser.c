/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-parser.c
 *
 * Implementation of GLL parser module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gll-lexer.h"

#include "gll-tokenset.h"


/* --------------------------------------------------------------------------
 * private type gll_parser_context_s
 * --------------------------------------------------------------------------
 * Record type to implement parser context.
 * ----------------------------------------------------------------------- */

typedef struct {
  /* lexer */ gll_lexer_t lexer;
  /* ast_root */ gll_ast_t ast_root;
  /* error_count */ uint_t error_count;
  /* status */ gll_parser_status_t status;
} gll_parser_context_s;


/* --------------------------------------------------------------------------
 * private type gll_parser_context_t
 * --------------------------------------------------------------------------
 * Pointer type to represent parser context.
 * ----------------------------------------------------------------------- */

typedef gll_parser_context_s *gll_parser_context_t;


/* --------------------------------------------------------------------------
 * function gll_syntax_check(filename, status)
 * --------------------------------------------------------------------------
 * Syntax checks a grammar specification .gll file represented by filename.
 * ----------------------------------------------------------------------- */

gll_parser_context_t specification (gll_parser_context_t p);

void gll_syntax_check
  (gll_string_t filename, gll_parser_status_t *status) {
  
  gll_parser_context_t p;
  gll_token_t lookahead;
  
  /* set up parser context */
  p = malloc(sizeof(gll_parser_context_s));
  
  if (p == NULL) {
    SET_STATUS(status, GLL_PARSER_STATUS_ALLOCATION_FAILED);
    
  } /* end if */
  
  /* create lexer object */
  gll_new_lexer(&(p->lexer), filename, NULL);
  
  if (p->lexer == NULL) {
    SET_STATUS(status, GLL_PARSER_STATUS_ALLOCATION_FAILED);
  } /* end if */
    
  /* init error counter */
  p->error_count = 0;
  
  /* get the first lookahead symbol */
  lookahead = gll_next_sym(p->lexer);
  
  /* start parsing */
  if (lookahead == TOKEN_GRAMMAR) {
    specification(p);
  }
  else /* not a grammar file */ {
    /* fatal error */
  } /* end if */
  
  /* pass back status and return */
  SET_STATUS(status, p->status);
  return;
} /* end gll_syntax_check */


/* --------------------------------------------------------------------------
 * function gll_parse(filename, status)
 * --------------------------------------------------------------------------
 * Parses a grammar specification .gll file and returns an AST.
 * ----------------------------------------------------------------------- */

gll_ast_t gll_parse
  (gll_string_t filename, gll_parser_status_t *status) {
  
  /* TO DO */
  
  return NULL;
} /* end gll_parse */


/* --------------------------------------------------------------------------
 * private function match_token(p, expected_token, skip_set)
 * --------------------------------------------------------------------------
 * Matches the lookahead symbol to expected_token and returns true if they
 * match.  If they don't match, a syntax error is reported, the error count
 * is incremented, symbols are consumed until the lookahead symbol matches
 * one of the symbols in skip_set and false is returned.
 * ----------------------------------------------------------------------- */

void syntax_error_w_token
  (gll_token_t expected_token,
   gll_token_t found_token,
   uint_t line, uint_t column);

bool match_token
  (gll_parser_context_t p,
   gll_token_t expected_token,
   gll_tokenset_t skip_set) {
  
  gll_token_t lookahead = gll_next_sym(p->lexer);
  
  if (lookahead == expected_token) {
    return true;
  }
  else /* no match */ {
    /* report error */
    gll_syntax_error_w_token
      (expected_token, lookahead,
       gll_current_line(p->lexer),
       gll_current_column(p->lexer));
    p->error_count++;
    
    /* skip symbols */
    while (!gll_tokenset_member(skip_set, lookahead)) {
      lookahead = gll_consume_sym(p->lexer);
    } /* end while */
    return false;
  } /* end if */
} /* end match_token */


/* --------------------------------------------------------------------------
 * private function match_set(p, expected_set, skip_set)
 * --------------------------------------------------------------------------
 * Matches the lookahead symbol to set expected_set and returns true if it
 * matches any of the tokens in the set.  If there is no match, a syntax
 * error is reported, the error count is incremented, symbols are consumed
 * until the lookahead symbol matches one of the symbols in skip_set and
 * false is returned.
 * ----------------------------------------------------------------------- */

void syntax_error_w_tokenset
  (gll_tokenset_t expected_tokenset,
   gll_token_t found_token,
   uint_t line, uint_t column);

bool match_set
  (gll_parser_context_t p,
   gll_tokenset_t expected_set,
   gll_tokenset_t skip_set) {
  
  gll_token_t lookahead = gll_next_sym(p->lexer);
  
  /* check if lookahead matches any token in expected_set */
  if (gll_tokenset_member(expected_set, lookahead)) {
    return true;
  }
  else /* no match */ {
    /* report error */
    gll_syntax_error_w_tokenset
      (expected_set, lookahead,
       gll_current_line(p->lexer),
       gll_current_column(p->lexer));
    p->error_count++;
    
    /* skip symbols */
    while (!gll_tokenset_member(skip_set, lookahead)) {
      lookahead = gll_consume_sym(p->lexer);
    } /* end while */
    return false;
  } /* end if */
} /* end match_set */


/* --------------------------------------------------------------------------
 * private procedure syntax_error_w_token(expected, found, line, col)
 * --------------------------------------------------------------------------
 * Prints a syntax error message to the console. The message has the form:
 * line: n, column: m, syntax error: expected A but found Z.
 * ----------------------------------------------------------------------- */

void syntax_error_w_token
  (gll_token_t expected_token,
   gll_token_t found_token,
   uint_t line, uint_t column) {
   
   /* TO DO */
   
} /* end syntax_error_w_token */


/* --------------------------------------------------------------------------
 * private procedure syntax_error_w_tokenset(expected_set, found, line, col)
 * --------------------------------------------------------------------------
 * Prints a syntax error message to the console. The message has the form:
 * line: n, column: m, syntax error: expected A, B, C ... but found Z.
 * ----------------------------------------------------------------------- */

void syntax_error_w_tokenset
  (gll_tokenset_t expected_set,
   gll_token_t found_token,
   uint_t line, uint_t column) {
   
   /* TO DO */
   
} /* end syntax_error_w_tokenset */


/* ************************************************************************ *
 * Grammar Specification Syntax                                             *
 * ************************************************************************ */


/* --------------------------------------------------------------------------
 * private procedure specification()
 * --------------------------------------------------------------------------
 * specification :=
 *   GRAMMAR ident ';' ( definition ';' )+ ENDG ident '.'
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t ident (gll_parser_context_t p);

gll_token_t definition (gll_parser_context_t p);

gll_token_t specification (gll_parser_context_t p) {
  gll_string_t grammar_ident_header, grammar_ident_end;
  gll_token_t lookahead;
  
  /* GRAMMAR */
  lookahead = gll_consume_sym(p->lexer);
  
  /* ident */
  if (match_set(p, FIRST(IDENT), SKIP_TO_FIRST_OF_DEFN)) {
    lookahead = ident(p);
    grammar_ident_header = gll_current_lexeme(p->lexer);
    
    /* ';' */
    if (match_token(p, TOKEN_SEMICOLON, SKIP_TO_FIRST_OF_DEFN)) {
      lookahead = gll_consume_sym(p->lexer);
    } /* end if */
  } /* end if */
  
  if (match_set(p, FIRST(DEFINITION), SKIP_TO_FIRST_OF_DEFN_OR_ENDG) {
    lookahead = definition(p);
    
    while ((lookahead == TOKEN_NONTERMINAL_IDENT) ||
           (lookahead == TOKEN_TERMINAL_IDENT) ||
           (lookahead == TOKEN_ALIAS) ||
           (lookahead == TOKEN_PERIOD) ||
           (lookahead == TOKEN_RESERVED)) {
      lookahead = definition(p);
    
    } /* end while */
  } /* end if */
  
  /* ENDG */
  if (match_token(p, TOKEN_END, SKIP_TO_EOF)) {
    lookahead = gll_consume_sym(p->lexer);
    
    /* ident */
    if (match_set(p, FIRST(IDENT), SKIP_TO_FIRST_OF_DEFN)) {
      lookahead = ident(p);
      grammar_ident_end = gll_current_lexeme(p->lexer);
      
      if (grammar_ident_header != grammar_ident_end) {
        /* error: grammar identifiers don't match */
      } /* end if */
      
      /* '.' */
      if (match_token(p, TOKEN_PERIOD, SKIP_TO_EOF)) {
        lookahead = gll_consume_sym(p->lexer);
      } /* end if */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end specification */


/* --------------------------------------------------------------------------
 * private procedure ident()
 * --------------------------------------------------------------------------
 * ident :=
 *   NonTerminalIdent | TerminalIdent
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t ident (gll_parser_context_t p) {
  
  gll_token_t lookahead = gll_next_sym(p->lexer);
  
  /* NonTerminalIdent */
  if (lookahead == TOKEN_NONTERM_IDENT) {
    lookahead = gll_consume_sym();
  }
  /* | TerminalIdent */
  else if (lookahead == TOKEN_TERMINAL_IDENT) {
    lookahead = gll_consume_sym();
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
  } /* end if */
  
  return lookahead;
} /* end ident */


/* --------------------------------------------------------------------------
 * private procedure definition()
 * --------------------------------------------------------------------------
 * definition :=
 *   RESERVED reswordList |
 *   ALIAS aliasDef |
 *   nonTerminalDef |
 *   terminalDef |
 *   fragmentDef
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t resword_list (gll_parser_context_t p);

gll_token_t alias_definition (gll_parser_context_t p);

gll_token_t non_terminal_definition (gll_parser_context_t p);

gll_token_t terminal_definition (gll_parser_context_t p);

gll_token_t fragment_definition (gll_parser_context_t p);

gll_token_t definition (gll_parser_context_t p) {
  gll_token_t lookahead;
  
  lookahead = gll_next_sym(p->lexer);
  
  switch (lookahead) {
    
    /* RESERVED reswordList */
    case TOKEN_RESERVED :
      /* RESERVED */
      lookahead = gll_consume_sym(p->lexer);
      
      /* reswordList */
      if (match_token(p, TOKEN_RESWORD_IDENT,
          SKIP_TO_FIRST_OF_DEFN_OR_ENDG)) {
        lookahead = resword_list(p);
      } /* end if */
      break;
      
    /* | ALIAS aliasDef */
    case TOKEN_ALIAS :
      /* ALIAS */
      lookahead = gll_consume_sym(p->lexer);
      
      /* aliasDef */
      if (match_set(p, FIRST(ALIAS_DEFINITION),
          SKIP_TO_FIRST_OF_DEFN_OR_ENDG)) {
        lookahead = alias_definition(p);
      } /* end if */
      break;
      
    /* | nonTerminalDef */
    case TOKEN_NONTERM_IDENT :
      lookahead = non_terminal_definition(p);
      break;
      
    /* | terminalDef */
    case TOKEN_TERMINAL_IDENT :
      lookahead = terminal_definition(p);
      break;
      
    /* | fragmentDef */
    case TOKEN_PERIOD :
      lookahead = fragment_definition(p);
      break;
      
    default : /* unreachable code */
      /* fatal error -- abort */
  } /* end switch */
  
  return lookahead;
} /* end definition */


/* --------------------------------------------------------------------------
 * private procedure resword_list()
 * --------------------------------------------------------------------------
 * reswordList :=
 *   reswordDef ( ',' reswordDef )*
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t resword_definition (gll_parser_context_t p);

gll_token_t reserved_words (gll_parser_context_t p) {
  gll_token_t lookahead;
  
  /* reswordDef */
  lookahead = resword_definition(p);
  
  /* ( ',' reswordDef )* */
  while (lookahead == TOKEN_COMMA) {
    /* ',' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* reswordDef */
    if (match_token(p, TOKEN_RESWORD_IDENT,
        SKIP_TO_FOLLOW_OF_RESWORD_LIST)) {
      lookahead = resword_definition(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end resword_list */


/* --------------------------------------------------------------------------
 * private procedure reswordDef()
 * --------------------------------------------------------------------------
 * reswordDef :=
 *   ReswordIdent ( '=' String )?
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t resword_definition (gll_parser_context_t p) {
  gll_string_t resword_alias, resword;
  gll_token_t lookahead;
  
  /* ReswordIdent */
  lookahead = gll_consume_sym(p->lexer);
  resword_alias = gll_current_lexeme(p->lexer);
  
  /* ( '=' reswordDef )? */
  if (lookahead == TOKEN_EQUAL) {
    /* ',' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* String */
    if (match_token(p, TOKEN_STRING,
        SKIP_TO_FOLLOW_OF_RESWORD_DEFN)) {
      lookahead = gll_consume_sym(p->lexer);
      resword = gll_current_lexeme(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end resword_definition */


/* --------------------------------------------------------------------------
 * private procedure alias_definition()
 * --------------------------------------------------------------------------
 * aliasDef :=
 *   nonTermAliasDef | termAliasDef
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t non_terminal_alias_definition (gll_parser_context_t p);

gll_token_t terminal_alias_definition (gll_parser_context_t p);

gll_token_t alias_definition (gll_parser_context_t p) {
  
  gll_token_t lookahead = gll_next_sym(p->lexer);
  
  /* nonTermAliasDef */
  if (gll_tokenset_member(FIRST(NON_TERMINAL_ALIAS_DEFINITION), lookahead) {
    lookahead = non_terminal_alias_definition(p);
  }
  /* | termAliasDef */
  else if (gll_tokenset_member(FIRST(TERMINAL_ALIAS_DEFINITION), lookahead) {
    lookahead = terminal_alias_definition(p);
  }
  else /* unreachable code */ {
    /* fatal error -- abort */
  } /* end if */
  
  return lookahead;
} /* end alias_definition */


/* --------------------------------------------------------------------------
 * private procedure non_terminal_alias_definition()
 * --------------------------------------------------------------------------
 * nonTermAliasDef :=
 *   nonTermAliasList '=' ident
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t non_terminal_alias_list (gll_parser_context_t p);

gll_token_t non_terminal_alias_definition (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
    
  /* nonTermAliasList */
  lookahead = non_terminal_alias_list(p);
  
  /* '=' */
  if (match_token(p, TOKEN_EQUAL, SKIP_TO_DEFINITION)) {
    lookahead = gll_consume_sym(p->lexer);
    
    /* ident */
    if (match_set(p, FIRST(IDENT), SKIP_TO_DEFINITION)) {
      lookahead = gll_consume_sym(p->lexer);
      ident = gll_current_lexeme(p->lexer);
      
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end non_terminal_alias_definition */


/* --------------------------------------------------------------------------
 * private procedure non_terminal_alias_list()
 * --------------------------------------------------------------------------
 * nonTermAliasList :=
 *   NonTerminalIdent ( ',' NonTerminalIdent )*
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t non_terminal_alias_list (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
  
  /* NonTerminalIdent */
  lookahead = gll_consume_sym(p->lexer);
  ident = gll_current_lexeme(p->lexer);
  /* TO DO: add ident to list structure */
  
  /* ( ',' NonTerminalIdent )* */
  while (lookahead == TOKEN_COMMA) {
    /* ',' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* NonTerminalIdent */
    if (match_token(p, TOKEN_NONTERMINAL_IDENT,
        FOLLOW(NON_TERMINAL_ALIAS_LIST))) {
      lookahead = gll_consume_sym(p->lexer);
      ident = gll_current_lexeme(p->lexer);
      /* TO DO: add ident to list structure */
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end non_terminal_alias_list */


/* --------------------------------------------------------------------------
 * private procedure terminal_alias_definition()
 * --------------------------------------------------------------------------
 * termAliasDef :=
 *   terminalAliasList '=' ( TerminalIdent | literal )
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t terminal_alias_list (gll_parser_context_t p);

gll_token_t literal (gll_parser_context_t p);

gll_token_t terminal_alias_definition (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
    
  /* terminalAliasList */
  lookahead = terminal_alias_list(p);
  
  /* '=' */
  if (match_token(p, TOKEN_EQUAL, SKIP_TO_DEFINITION)) {
    lookahead = gll_consume_sym(p->lexer);
    
    /* ( TerminalIdent */
    if (lookahead == TOKEN_TERMINAL_IDENT) {
      lookahead = gll_consume_sym(p->lexer);
      ident = gll_current_lexeme(p->lexer);
    }
    /* | literal ) */
    else if (gll_tokenset_member(FIRST(LITERAL), lookahead)) {
      lookahead = literal(p);
    }
    else /* unreachable code */ {
      /* fatal error -- abort */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end terminal_alias_definition */


/* --------------------------------------------------------------------------
 * private procedure terminal_alias_list()
 * --------------------------------------------------------------------------
 * terminalAliasList :=
 *   TerminalIdent ( ',' TerminalIdent )*
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t terminal_alias_list (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
  
  /* TerminalIdent */
  lookahead = gll_consume_sym(p->lexer);
  ident = gll_current_lexeme(p->lexer);
  /* TO DO: add ident to list structure */
  
  /* ( ',' TerminalIdent )* */
  while (lookahead == TOKEN_COMMA) {
    /* ',' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* TerminalIdent */
    if (match_token(p, TOKEN_TERMINAL_IDENT,
        FOLLOW(TERMINAL_ALIAS_LIST))) {
      lookahead = gll_consume_sym(p->lexer);
      ident = gll_current_lexeme(p->lexer);
      /* TO DO: add ident to list structure */
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end terminal_alias_list */


/* --------------------------------------------------------------------------
 * private procedure literal()
 * --------------------------------------------------------------------------
 * literal :=
 *   String | QuotedLowerLetter | QuotedUpperLetter |
 *   QuotedDigit | QuotedNonAlphaNum | CharCode | EOF
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t literal (gll_parser_context_t p) {
  gll_string_t lexeme;
  gll_token_t lookahead;
  
  lookahead = gll_next_sym(p->lexer);
  
  switch (lookahead) {
  
    /* String */
    case TOKEN_STRING :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | QuotedLowerLetter */
    case TOKEN_QUOTED_LOWER :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
    
    /* | QuotedUpperLetter */
    case TOKEN_QUOTED_UPPER :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | QuotedDigit */
    case TOKEN_QUOTED_DIGIT :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | QuotedNonAlphaNum */
    case TOKEN_QUOTED_NONALNUM :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | CharCode */
    case TOKEN_CHAR_CODE_LITERAL :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | EOF */
    case TOKEN_EOF :
      lookahead = gll_consume_sym(p->lexer);
      break;
    
    default : /* unreachable code */
      /* fatal error -- abort */
  } /* end switch */
  
  return lookahead;
} /* end literal */


/* --------------------------------------------------------------------------
 * private procedure non_terminal_definition()
 * --------------------------------------------------------------------------
 * nonTerminalDef :=
 *   NonTerminalIdent ':=' expression+
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t expression (gll_parser_context_t p);

gll_token_t non_terminal_definition (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
    
  /* NonTerminalIdent */
  lookahead = gll_consume_sym(p->lexer);
  ident = gll_current_lexeme(p->lexer);
  
  /* ':=' */
  if (match_token(p, TOKEN_COLONEQUAL, FOLLOW(NON_TERMINAL_DEFINITION))) {
    lookahead = gll_consume_sym(p->lexer);
    
    /* expression+ */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(EXPRESSION))) {
      lokahead = expression(p);
    
      while (gll_tokenset_member(FIRST(EXPRESSION), lookahead)) {
        lokahead = expression(p);
      } /* end while */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end non_terminal_definition */


/* --------------------------------------------------------------------------
 * private procedure expression()
 * --------------------------------------------------------------------------
 * expression :=
 *   term ( '|' term )*
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t term (gll_parser_context_t p);

gll_token_t expression (gll_parser_context_t p) {
  gll_token_t lookahead;
  
  /* term */
  lookahead = term(p);
  
  /* ( '|' term )* */
  while (lookahead == TOKEN_BAR) {
    /* '|' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* term */
    if (match_set(p, FIRST(TERM), FOLLOW(EXPRESSION)) {
      lookahead = term(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end expression */


/* --------------------------------------------------------------------------
 * private procedure term()
 * --------------------------------------------------------------------------
 * term :=
 *   factor ( '*' | '+' | '?' )? | literalOrRange
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t factor (gll_parser_context_t p);

gll_token_t literal_or_range (gll_parser_context_t p);

gll_token_t term (gll_parser_context_t p) {
  gll_token_t lookahead;
  
  /* literal */
  if (gll_tokenset_element(FIRST(LITERAL_OR_RANGE)) {
    lookahead = literal_or_range(p);
  }
  else /* | factor */ {
    lookahead = factor(p);
    
    /* ( '*' | '+' | '?' )? */
    switch (lookahead) {
      case '*' :
        gll_consume_sym(p->lexer);
        break;
        
      case '+' :
        gll_consume_sym(p->lexer);
        break;
      
      case '?' :
        gll_consume_sym(p->lexer);
        break;
      
    } /* end switch */
  } /* end if */
  
  return lookahead;
} /* end term */


/* --------------------------------------------------------------------------
 * private procedure factor()
 * --------------------------------------------------------------------------
 * factor :=
 *   NonTerminalIdent | TerminalIdent | ReswordIdent | '(' expression+ ')'
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t factor (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
  
  lookahead = gll_next_char(p->lexer);
  
  switch (lookahead) {
  
  /* NonTerminalIdent */
  case TOKEN_NONTERMINAL_IDENT :
    lookahead = gll_consume_sym(p->lexer);
    ident = gll_current_lexeme(p->lexer);
    break;
  
  /* | TerminalIdent */
  case TOKEN_TERMINAL_IDENT :
    lookahead = gll_consume_sym(p->lexer);
    ident = gll_current_lexeme(p->lexer);
    break;
  
  /* | ReswordIdent */
  case TOKEN_RESWORD_IDENT :
    lookahead = gll_consume_sym(p->lexer);
    ident = gll_current_lexeme(p->lexer);
    break;
  
  /* | '(' terminalExpression+ ')' */
  case TOKEN_LEFT_PAREN :
    /* '(' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* expression+ */
    if (match_set(p, FIRST(EXPRESSION), FOLLOW(FACTOR))) {
      lookahead = expression(p);
      
      while (gll_tokenset_member(FIRST(EXPRESSION), lookahead)) {
        lookahead = expression(p);
      } /* end while */
      
      /* ')' */
      if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(FACTOR))) {
        lookahead = gll_consume_sym(p->lexer);
      } /* end if */
    } /* end if */
    break;
  
    default : /* unreachable code */
      /* fatal error -- abort */
  } /* end switch */
  
  return lookahead;
} /* end factor */


/* --------------------------------------------------------------------------
 * private procedure terminal_definition()
 * --------------------------------------------------------------------------
 * terminalDef :=
 *   TerminalIdent ':=' terminalExpression+
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t terminal_expression (gll_parser_context_t p);

gll_token_t terminal_definition (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
    
  /* TerminalIdent */
  lookahead = gll_consume_sym(p->lexer);
  ident = gll_current_lexeme(p->lexer);
  
  /* ':=' */
  if (match_token(p, TOKEN_COLONEQUAL, SKIP_TO_FIRST_DEFINITION)) {
    lookahead = gll_consume_sym(p->lexer);
    
    /* terminalExpression+ */
    if (match_set(p, FIRST(TERMINAL_EXPRESSION),
        FOLLOW(TERMINAL_EXPRESSION))) {
      lokahead = terminal_expression(p);
    
      while (gll_tokenset_member(FIRST(TERMINAL_EXPRESSION), lookahead)) {
        lokahead = terminal_expression(p);
      } /* end while */
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end terminal_definition */


/* --------------------------------------------------------------------------
 * private procedure terminal_expression()
 * --------------------------------------------------------------------------
 * terminalExpression :=
 *   terminalTerm ( '|' terminalTerm )*
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t terminal_term (gll_parser_context_t p);

gll_token_t terminal_expression (gll_parser_context_t p) {
  gll_token_t lookahead;
  
  /* terminalTerm */
  lookahead = terminal_term(p);
  
  /* ( '|' terminalTerm )* */
  while (lookahead == TOKEN_BAR) {
    /* '|' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* terminalTerm */
    if (match_set(p, FIRST(TERMINAL_TERM), FOLLOW(TERMINAL_EXPRESSION)) {
      lookahead = terminal_term(p);
    } /* end if */
  } /* end while */
  
  return lookahead;
} /* end terminal_expression */


/* --------------------------------------------------------------------------
 * private procedure terminal_term()
 * --------------------------------------------------------------------------
 * terminalTerm :=
 *   terminalFactor ( '*' | '+' | '?' )? | literalOrRange
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t terminal_factor (gll_parser_context_t p);

gll_token_t terminal_term (gll_parser_context_t p) {
  gll_token_t lookahead;
  
  /* literalOrRange */
  if (gll_tokenset_element(FIRST(LITERAL_OR_RANGE)) {
    lookahead = literal_or_range(p);
  }
  else /* | terminalFactor */ {
    lookahead = terminal_factor(p);
    
    /* ( '*' | '+' | '?' )? */
    switch (lookahead) {
      case '*' :
        gll_consume_sym(p->lexer);
        break;
        
      case '+' :
        gll_consume_sym(p->lexer);
        break;
      
      case '?' :
        gll_consume_sym(p->lexer);
        break;
      
    } /* end switch */
  } /* end if */
  
  return lookahead;
} /* end terminal_term */


/* --------------------------------------------------------------------------
 * private procedure terminal_factor()
 * --------------------------------------------------------------------------
 * terminalFactor :=
 *   TerminalIdent | '(' terminalExpression+ ')'
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t terminal_factor (gll_parser_context_t p) {
  gll_string_t ident;
  gll_token_t lookahead;
  
  lookahead = gll_next_char(p->lexer);
  
  switch (lookahead) {
  
    /* TerminalIdent */
    case TOKEN_TERMINAL_IDENT :
      lookahead = gll_consume_sym(p->lexer);
      ident = gll_current_lexeme(p->lexer);
      break;
  
    /* | '(' terminalExpression+ ')' */
    case TOKEN_LEFT_PAREN :
      /* '(' */
      lookahead = gll_consume_sym(p->lexer);
    
      /* terminalExpression+ */
      if (match_set(p, FIRST(TERMINAL_EXPRESSION), FOLLOW(TERMINAL_FACTOR))) {
        lookahead = terminal_expression(p);
      
        while (gll_tokenset_member(FIRST(TERMINAL_EXPRESSION), lookahead)) {
          lookahead = terminal_expression(p);
        } /* end while */
      
        /* ')' */
        if (match_token(p, TOKEN_RIGHT_PAREN, FOLLOW(TERMINAL_FACTOR))) {
          lookahead = gll_consume_sym(p->lexer);
        } /* end if */
      } /* end if */
      break;
  
    default : /* unreachable code */
      /* fatal error -- abort */
  } /* end switch */
  
  return lookahead;
} /* end terminal_factor */


/* --------------------------------------------------------------------------
 * private procedure fragment_definition()
 * --------------------------------------------------------------------------
 * fragmentDef :=
 *   '.' terminalDef
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t fragment_definition (gll_parser_context_t p) {
  
  gll_token_t lookahead = gll_next_sym(p->lexer);
  
  /* '.' */
  lookahead = gll_consume_sym();
  
  /* terminalDef */
  if (match_set(p, FIRST(TERMINAL_DEFINITION),
      FOLLOW(FRAGMENT_DEFINITION))) {
    lookahead = terminal_definition(p);
  } /* end if */
  
  return lookahead;
} /* end fragment_definition */


/* --------------------------------------------------------------------------
 * private procedure literal_or_range()
 * --------------------------------------------------------------------------
 * literalOrRange :=
 *   String | quotedLowerCharOrRange | quotedUpperCharOrRange |
 *   quotedDigitOrRange | charCodeOrRange | QuotedAlphaNum | EOF
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t quoted_lower_letter_or_range (gll_parser_context_t p);

gll_token_t quoted_upper_letter_or_range (gll_parser_context_t p);

gll_token_t quoted_digit_or_range (gll_parser_context_t p);

gll_token_t char_code_or_range (gll_parser_context_t p);

gll_token_t literal_or_range (gll_parser_context_t p) {
  gll_string_t lexeme;
  gll_token_t lookahead;
  
  lookahead = gll_next_sym(p->lexer);
  
  switch (lookahead) {
  
    /* String */
    case TOKEN_STRING :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | quotedLowerCharOrRange */
    case TOKEN_QUOTED_LOWER :
      lookahead = quoted_lower_letter_or_range(p);
      break;
    
    /* | quotedUpperCharOrRange */
    case TOKEN_QUOTED_UPPER :
      lookahead = quoted_upper_letter_or_range(p);
      break;
      
    /* | quotedDigitOrRange */
    case TOKEN_QUOTED_DIGIT :
      lookahead = quoted_digit_or_range(p);
      break;
      
    /* | charCodeOrRange */
    case TOKEN_CHAR_CODE_LITERAL :
      lookahead = char_code_or_range(p);
      break;
      
    /* | QuotedAlphaNum */
    case TOKEN_CHAR_CODE_LITERAL :
      lookahead = gll_consume_sym(p->lexer);
      lexeme = gll_current_lexeme(p->lexer);
      break;
      
    /* | EOF */
    case TOKEN_EOF :
      lookahead = gll_consume_sym(p->lexer);
      break;
    
    default : /* unreachable code */
      /* fatal error -- abort */
  } /* end switch */
  
  return lookahead;
} /* end literal_or_range */


/* --------------------------------------------------------------------------
 * private procedure quoted_lower_letter_or_range()
 * --------------------------------------------------------------------------
 * quotedLowerLetterOrRange :=
 *   QuotedLowerLetter ( '..' QuotedLowerLetter )?
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t quoted_lower_letter_or_range (gll_parser_context_t p) {
  gll_string_t first_letter, second_letter;
  gll_token_t lookahead;
  
  /* QuotedLowerLetter */
  lookahead = gll_consume_sym(p->lexer);
  first_letter = gll_current_lexeme(p->lexer);
  
  /* ( '..' QuotedLowerLetter )? */
  if (lookahead == TOKEN_DOTDOT) {
    /* '..' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* QuotedLowerLetter */
    if (match_token(p, TOKEN_QUOTED_LOWER, FOLLOW(TERM))) {
      lookahead = gll_consume_sym(p->lexer);
      second_letter = gll_current_lexeme(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end quoted_lower_letter_or_range */


/* --------------------------------------------------------------------------
 * private procedure quoted_upper_letter_or_range()
 * --------------------------------------------------------------------------
 * quotedUpperLetterOrRange :=
 *   QuotedUpperLetter ( '..' QuotedUpperLetter )?
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t quoted_upper_letter_or_range (gll_parser_context_t p) {
  gll_string_t first_letter, second_letter;
  gll_token_t lookahead;
  
  /* QuotedUpperLetter */
  lookahead = gll_consume_sym(p->lexer);
  first_letter = gll_current_lexeme(p->lexer);
  
  /* ( '..' QuotedUpperLetter )? */
  if (lookahead == TOKEN_DOTDOT) {
    /* '..' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* QuotedUpperLetter */
    if (match_token(p, TOKEN_QUOTED_UPPER, FOLLOW(TERM))) {
      lookahead = gll_consume_sym(p->lexer);
      second_letter = gll_current_lexeme(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end quoted_upper_letter_or_range */


/* --------------------------------------------------------------------------
 * private procedure quoted_digit_or_range()
 * --------------------------------------------------------------------------
 * QuotedDigitOrRange :=
 *   QuotedDigit ( '..' QuotedDigit )?
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t quoted_digit_or_range (gll_parser_context_t p) {
  gll_string_t first_digit, second_digit;
  gll_token_t lookahead;
  
  /* QuotedDigit */
  lookahead = gll_consume_sym(p->lexer);
  first_letter = gll_current_lexeme(p->lexer);
  
  /* ( '..' QuotedDigit )? */
  if (lookahead == TOKEN_DOTDOT) {
    /* '..' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* QuotedDigit */
    if (match_token(p, TOKEN_QUOTED_DIGIT, FOLLOW(TERM))) {
      lookahead = gll_consume_sym(p->lexer);
      second_letter = gll_current_lexeme(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end quoted_digit_or_range */


/* --------------------------------------------------------------------------
 * private procedure char_code_or_range()
 * --------------------------------------------------------------------------
 * charCodeOrRange :=
 *   CharCode ( '..' CharCode )?
 *   ;
 * ----------------------------------------------------------------------- */

gll_token_t char_code_or_range (gll_parser_context_t p) {
  gll_string_t first_char_code, second_char_code;
  gll_token_t lookahead;
  
  /* CharCode */
  lookahead = gll_consume_sym(p->lexer);
  first_char_code = gll_current_lexeme(p->lexer);
  
  /* ( '..' CharCode )? */
  if (lookahead == TOKEN_DOTDOT) {
    /* '..' */
    lookahead = gll_consume_sym(p->lexer);
    
    /* CharCode */
    if (match_token(p, TOKEN_CHAR_CODE, FOLLOW(TERM))) {
      lookahead = gll_consume_sym(p->lexer);
      second_char_code = gll_current_lexeme(p->lexer);
    } /* end if */
  } /* end if */
  
  return lookahead;
} /* end char_code_or_range */

/* END OF FILE */