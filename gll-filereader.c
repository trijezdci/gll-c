/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-filereader.h
 *
 * Implementation of GLL input file reader module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gll-filereader.h"
#include "gll-common.h"
#include "gll-fileutils.h"

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stddef.h>


/* --------------------------------------------------------------------------
 * private type gll_infile_struct_t
 * --------------------------------------------------------------------------
 * record type representing a Modula-2 source file.
 * ----------------------------------------------------------------------- */

struct gll_infile_struct_t {
  /* file */         FILE *file;
  /* filename */     gll_string_t filename;
  /* index */        size_t index;
  /* line */         uint_t line;
  /* column */       uint_t column;
  /* marker_set */   bool marker_set;
  /* marker_index */ size_t marked_index;
  /* status */       gll_infile_status_t status;
  /* buflen */       size_t buflen;
  /* buffer */       char buffer[];
};

typedef struct gll_infile_struct_t gll_infile_struct_t;


/* --------------------------------------------------------------------------
 * procedure gll_open_infile(infile, filename, status)
 * --------------------------------------------------------------------------
 * Allocates a new object of type gll_infile_t, opens an input file and
 * associates the opened file with the newly created infile object.
 *
 * pre-conditions:
 * o  parameter file must be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  pointer to newly allocated and opened file is passed back in infile
 * o  line and column counters of the newly allocated infile are set to 1
 * o  GLL_INFILE_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if infile is not NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * o  if the file represented by filename cannot be found
 *    status GLL_INFILE_STATUS_FILE_NOT_FOUND is returned
 * o  if the file represented by filename cannot be accessed
 *    status GLL_INFILE_STATUS_FILE_ACCESS_DENIED is returned
 * o  if no infile object could be allocated
 *    status GLL_INFILE_STATUS_ALLOCATION_FAILED is returned
 * --------------------------------------------------------------------------
 */

/* TO DO : Check for file size limit */

gll_infile_t gll_open_infile
  (gll_string_t filename, gll_infile_status_t *status) {
  
  FILE *file; size_t size;
  gll_infile_t new_infile;
  
  /* check pre-conditions */
  if (filename == NULL) {
    SET_STATUS(status, GLL_INFILE_STATUS_INVALID_REFERENCE);    
    return NULL;
  } /* end if */
  
  /* open file */
  file = fopen(gll_string_char_ptr(filename), "r");
  
  /* if operation failed, pass back status and return */
  if (file == NULL) {
    if (status != NULL) {
      if ((errno == ENOENT) ||
          (errno == ENOTDIR) ||
          (errno == ENAMETOOLONG)) {
        *status = GLL_INFILE_STATUS_FILE_NOT_FOUND;
      }
      else if (errno == EACCES) {
        *status = GLL_INFILE_STATUS_FILE_ACCESS_DENIED;
      }
      else if (errno == ENOMEM) {
        *status = GLL_INFILE_STATUS_ALLOCATION_FAILED;
      }
      else {
        *status = GLL_INFILE_STATUS_IO_SUBSYSTEM_ERROR;
      } //
    } /* end if */
    return NULL;
  } /* end if */
  
  /* allocate new infile */
  size = filesize(gll_string_char_ptr(filename));
  new_infile = malloc(sizeof(gll_infile_struct_t) + size + 1);
  
  /* if allocation failed, close file, pass status and return */
  if (new_infile == NULL) {
    fclose(file);
    
    SET_STATUS(status, GLL_INFILE_STATUS_ALLOCATION_FAILED);    
    return NULL;
  } /* end if */
  
  /* read file contents into buffer */
  new_infile->buflen = fread(&new_infile->buffer, sizeof(char), size, file);
  
  /* if file empty, close file, deallocate infile, pass status and return */
  if (new_infile->buflen == 0) {
    free(new_infile);
    fclose(file);
    
    SET_STATUS(status, GLL_INFILE_STATUS_FILE_EMPTY);
    return NULL;
  } /* end if */
  
  /* initialise newly allocated infile */
  new_infile->file = file;
  new_infile->filename = filename;
  new_infile->index = 0;
  new_infile->line = 1;
  new_infile->column = 1;
  new_infile->marker_set = false;
  new_infile->marked_index = 0;
  new_infile->status = GLL_INFILE_STATUS_SUCCESS;
  
  return new_infile;
} /* gll_open_infile */


/* --------------------------------------------------------------------------
 * function gll_read_char(infile)
 * --------------------------------------------------------------------------
 * Reads the lookahead character from infile, advancing the current reading
 * position, updating line and column counter and returns its character code.
 * Returns EOF if the lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

/* TO DO : Check for line and column counter limit */

int gll_read_char (gll_infile_t infile) {
  char ch;

  /* check pre-conditions */
  if (infile == NULL) {
    return ASCII_NUL;
  } /* end if */
  
  if (infile->index == infile->buflen) {
    infile->status = GLL_INFILE_STATUS_ATTEMPT_TO_READ_PAST_EOF;
    return EOF;
  } /* end if */
  
  ch = infile->buffer[infile->index];
  infile->index++;
  
  /* if new line encountered, update line and column counters */
  if (ch == ASCII_LF) {
    infile->line++;
    infile->column = 1;
  }
  else if (ch == ASCII_CR) {
    infile->line++;
    infile->column = 1;
    
    /* if LF follows, skip it */
    if ((infile->index < infile->buflen) &&
        (infile->buffer[infile->index] == ASCII_LF)) {
      infile->index++;
    } /* end if */
    
    ch = ASCII_LF;
  }
  else {
    infile->column++;
  } /* end if */
  
  infile->status = GLL_INFILE_STATUS_SUCCESS;
  return ch;
} /* end gll_read_char */


/* --------------------------------------------------------------------------
 * function gll_mark_lexeme(infile)
 * --------------------------------------------------------------------------
 * Marks the current lookahead character as the start of a lexeme.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  position of lookahead character is stored internally
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

void gll_mark_lexeme (gll_infile_t infile) {
  
  /* check pre-conditions */
  if (infile == NULL) {
    return;
  } /* end if */
  
  /* set marker */
  infile->marker_set = true;
  infile->marked_index = infile->index;
  
  return;
} /* end gll_mark_lexeme */


/* --------------------------------------------------------------------------
 * function gll_read_marked_lexeme(infile)
 * --------------------------------------------------------------------------
 * Returns a string object with the character sequence starting with the
 * character that has been marked using procedure gll_mark_lexeme() and
 * ending with the last consumed character.  Returns NULL if no marker
 * has been set or if the marked character has not been consumed.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  marked position is cleared
 * o  dynamic string with lexeme is returned
 *
 * error-conditions:
 * o  if infile is NULL upon entry,
 *    no operation is carried out and NULL is returned
 * --------------------------------------------------------------------------
 */

gll_string_t gll_read_marked_lexeme (gll_infile_t infile) {
  gll_string_t lexeme;
  unsigned int length;
  gll_string_status_t status;
  
  /* check pre-conditions */
  if ((!infile->marker_set) || (infile->marked_index == infile->index)) {
    return NULL;
  } /* end if */
  
  /* determine length */
  length = infile->index - infile->marked_index;
  
  /* copy lexeme */
  lexeme = gll_get_string_for_slice
    (infile->buffer, infile->marked_index, length, &status);
  
  if (status == GLL_STRING_STATUS_ALLOCATION_FAILED) {
    infile->status = GLL_INFILE_STATUS_ALLOCATION_FAILED;
    return NULL;
  } /* end if */
  
  /* clear marker */
  infile->marker_set = false;
  
  return lexeme;
} /* end gll_read_marked_lexeme */


/* --------------------------------------------------------------------------
 * function gll_consume_char(infile)
 * --------------------------------------------------------------------------
 * Consumes the current lookahead character, advancing the current reading
 * position, updating line and column counter and returns the character code
 * of the new lookahead character that follows the consumed character.
 * Returns EOF if the lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

inline int gll_consume_char (gll_infile_t infile) {
  
  /* consume lookahead character */
  gll_read_char(infile);
  
  /* return new lookahead character */
  return gll_next_char(infile);
} /* end gll_consume_char */


/* --------------------------------------------------------------------------
 * function gll_next_char(infile)
 * --------------------------------------------------------------------------
 * Reads the lookahead character from infile without advancing the current
 * reading position and returns its character code.  Returns EOF if the
 * lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are NOT updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

int gll_next_char (gll_infile_t infile) {
  char ch;

  /* check pre-conditions */
  if (infile == NULL) {
    return ASCII_NUL;
  } /* end if */
  
  if (infile->index == infile->buflen) {
    infile->status = GLL_INFILE_STATUS_ATTEMPT_TO_READ_PAST_EOF;
    return EOF;
  } /* end if */
  
  ch = infile->buffer[infile->index];
  
  /* return LF for CR */
  if (ch == ASCII_CR) {
    ch = ASCII_LF;
  } /* end if */
  
  infile->status = GLL_INFILE_STATUS_SUCCESS;
  return ch;  
} /* end gll_next_char */


/* --------------------------------------------------------------------------
 * function gll_la2_char(infile)
 * --------------------------------------------------------------------------
 * Reads the second lookahead character from infile without advancing the
 * current reading position and returns its character code.  Returns EOF
 * if the second lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of second lookahead character or EOF is returned
 * o  current reading position and line and column counters are NOT updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

int gll_la2_char (gll_infile_t infile) {
  char la2;
  size_t index;

  /* check pre-conditions */
  if (infile == NULL) {
    return ASCII_NUL;
  } /* end if */
  
  if (index+1 == infile->buflen) {
    infile->status = GLL_INFILE_STATUS_ATTEMPT_TO_READ_PAST_EOF;
    return EOF;
  } /* end if */
  
  la2 = infile->buffer[infile->index+1];
  
  /* skip CR LF sequence if encountered */
  if ((infile->buffer[infile->index] == ASCII_CR) && (la2 == ASCII_LF)) {
    if (infile->index+2 == infile->buflen) {
      infile->status = GLL_INFILE_STATUS_ATTEMPT_TO_READ_PAST_EOF;
      return EOF;
    } /* end if */
    
    la2 = infile->buffer[infile->index+2];
  } /* end if */
  
  /* return LF for CR */
  if (la2 == ASCII_CR) {
    la2 = ASCII_LF;
  } /* end if */
    
  infile->status = GLL_INFILE_STATUS_SUCCESS;
  return la2;
} /* end gll_la2_char */


/* --------------------------------------------------------------------------
 * function gll_infile_filename(infile)
 * --------------------------------------------------------------------------
 * Returns the filename associated with infile.
 * --------------------------------------------------------------------------
 */

gll_string_t gll_infile_filename (gll_infile_t infile) {

  /* check pre-conditions */
  if (infile == NULL) {
    return NULL;
  } /* end if */
  
  return infile->filename;
}  /* end gll_infile_status */


/* --------------------------------------------------------------------------
 * function gll_infile_status(infile)
 * --------------------------------------------------------------------------
 * Returns the status of the last operation on file.
 * --------------------------------------------------------------------------
 */

gll_infile_status_t gll_infile_status (gll_infile_t infile) {

  /* check pre-conditions */
  if (infile == NULL) {
    return GLL_INFILE_STATUS_INVALID_REFERENCE;
  } /* end if */
  
  return infile->status;
}  /* end gll_infile_status */


/* --------------------------------------------------------------------------
 * function gll_infile_eof(infile)
 * --------------------------------------------------------------------------
 * Returns true if the current reading position of infile lies beyond the end
 * of the associated file, returns false otherwise.
 * --------------------------------------------------------------------------
 */

bool gll_infile_eof (gll_infile_t infile) {

  /* check pre-conditions */
  if (infile == NULL) {
    return true;
  } /* end if */
  
  return (infile->index == infile->buflen);
} /* end gll_infile_eof */


/* --------------------------------------------------------------------------
 * function gll_infile_current_line(infile)
 * --------------------------------------------------------------------------
 * Returns the current line counter of infile.
 * --------------------------------------------------------------------------
 */

unsigned int gll_infile_current_line (gll_infile_t infile) {

  /* check pre-conditions */
  if (infile == NULL) {
    return 0;
  } /* end if */
  
  return infile->line;
} /* end gll_infile_current_line */


/* --------------------------------------------------------------------------
 * function gll_infile_current_column(infile)
 * --------------------------------------------------------------------------
 * Returns the current column counter of infile.
 * --------------------------------------------------------------------------
 */

unsigned int gll_infile_current_column (gll_infile_t infile) {

  /* check pre-conditions */
  if (infile == NULL) {
    return 0;
  } /* end if */
  
  return infile->column;
} /* end gll_infile_current_column */


/* --------------------------------------------------------------------------
 * procedure gll_close_infile(file, status)
 * --------------------------------------------------------------------------
 * Closes the file associated with handle file, deallocates its file object
 * and returns NULL in handle file.
 *
 * pre-conditions:
 * o  parameter file must not be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  file object is deallocated
 * o  NULL is passed back in file
 * o  GLL_INFILE_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if file is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * -------------------------------------------------------------------------- 
 */

void gll_close_infile (gll_infile_t *infptr, gll_infile_status_t *status) {
  gll_infile_t infile;
  
  /* check pre-conditions */
  if (infptr == NULL) {
    SET_STATUS(status, GLL_INFILE_STATUS_INVALID_REFERENCE);
    return;
  } /* end if */
  
  infile = *infptr;
  
  fclose(infile->file);
  free(infile);
  *infptr = NULL;
  
  SET_STATUS(status, GLL_INFILE_STATUS_SUCCESS);
  return;
} /* end gll_close_infile */

/* END OF FILE */