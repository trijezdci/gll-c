/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-token.c
 *
 * Implementation of GLL token module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gll-token.h"

#include <stddef.h>


/* --------------------------------------------------------------------------
 * array gll_token_name_table
 * --------------------------------------------------------------------------
 * Human readable names for tokens.
 * --------------------------------------------------------------------------
 */

static const char *gll_token_name_table[] = {
  /* Null Token */
  
  "UNKNOWN",
  
  /* Reserved Words */

  "RW-ALIAS\0",
  "RW-EOF\0",
  "RW-ENDG\0",
  "RW-GRAMMAR\0",
  "RW-RESERVED\0",
  
  /* Identifiers */
  
  "LOWER-IDENT\0",
  "UPPER-IDENT\0",
  
  /* Literals */
  
  "STRING-LITERAL\0",
  "QUOTED-LOWER-LETTER\0",
  "QUOTED-UPPER-LETTER\0",
  "QUOTED-DECIMAL-DIGIT\0",
  "QUOTED-NON-ALNUM-CHAR\0",
  
  "MALFORMED-LITERAL\0",
  
  /* Special Symbols */
  
  "ASTERISK\0",
  "PLUS\0",
  "COMMA\0",
  "PERIOD\0",
  "DOTDOT\0",
  "COLON-EQUAL\0",
  "SEMICOLON\0",
  "EQUAL\0",
  "QMARK\0",
  "VERTICAL-BAR\0",
  "LEFT-PAREN\0",
  "RIGHT-PAREN\0",
  "END-OF-FILE\0",
  
  /* out-of-range guard */
  
  "\0"

}; /* end gll_token_name_table */


/* --------------------------------------------------------------------------
 * array gll_resword_lexeme_table
 * --------------------------------------------------------------------------
 * Lexeme strings for reserved word tokens.
 * --------------------------------------------------------------------------
 */

static const char *gll_resword_lexeme_table[] = {
  
  /* dummy */
  "\0",
  
  /* lexemes */
  "alias\0",
  "EOF\0",
  "endg\0",
  "grammar\0",
  "reserved\0",

}; /* end gll_resword_lexeme_table */


/* --------------------------------------------------------------------------
 * array gll_special_symbol_lexeme_table
 * --------------------------------------------------------------------------
 * Lexeme strings for special symbol tokens.
 * --------------------------------------------------------------------------
 */

static const char *gll_special_symbol_lexeme_table[] = {
  
  "*\0",  /* ASTERISK */
  "+\0",  /* PLUS */
  ",\0",  /* COMMA */
  ".\0",  /* PERIOD */
  "..\0", /* DOTDOT */
  ":=\0", /* COLON-EQUAL */
  ";\0",  /* SEMICOLON */
  "=\0",  /* EQUAL */ 
  "?\0",  /* QMARK */         
  "|\0",  /* VERTICAL-BAR */ 
  "(\0",  /* LEFT-PAREN */ 
  ")\0"   /* RIGHT-PAREN */ 
  
}; /* end gll_special_symbol_lexeme_table */


/* --------------------------------------------------------------------------
 * private function str_match(str1, str2)
 * --------------------------------------------------------------------------
 * Returns true character strings str1 and str2 match, otherwise false.
 * --------------------------------------------------------------------------
 */

#define MAX_RESWORD_LENGTH 8

bool str_match (const char *str1, const char *str2) {
  uint_t index;
  
  index = 0;
  while (index <= MAX_RESWORD_LENGTH + 1) {
    if (str1[index] != str2[index]) {
      return false;
    }
    else if (str1[index] == '\0') {
      return true;
    }
    else {
      index++;
    } /* end if */
  } /* end while */
  
  return false;
} /* end str_match */


/* --------------------------------------------------------------------------
 * function gll_is_valid_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a terminal symbol, otherwise false.
 * --------------------------------------------------------------------------
 */

inline bool gll_is_valid_token (gll_token_t token) {
  return ((token > TOKEN_UNKNOWN) && (token < TOKEN_END_MARK));
} /* end  */


/* --------------------------------------------------------------------------
 * function gll_is_resword_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a reserved word, otherwise false.
 * --------------------------------------------------------------------------
 */

inline bool gll_is_resword_token (gll_token_t token) {
  return ((token >= FIRST_RESERVED_WORD_TOKEN) &&
          (token <= LAST_RESERVED_WORD_TOKEN));
} /* end gll_is_resword_token */


/* --------------------------------------------------------------------------
 * function gll_is_literal_token(token)
 * --------------------------------------------------------------------------
 * Returns true if token represents a literal, otherwise false.
 * --------------------------------------------------------------------------
 */

inline bool gll_is_literal_token (gll_token_t token) {
  return ((token >= FIRST_LITERAL_TOKEN) &&
          (token <= LAST_LITERAL_TOKEN));
} /* end gll_is_literal_token */


/* --------------------------------------------------------------------------
 * function gll_is_special_symbol_token(token)
 * --------------------------------------------------------------------------
 * Returns TRUE if token represents a special symbol, otherwise FALSE.
 * --------------------------------------------------------------------------
 */

inline bool gll_is_special_symbol_token (gll_token_t token) {
  return ((token >= FIRST_SPECIAL_SYMBOL_TOKEN) &&
          (token <= LAST_SPECIAL_SYMBOL_TOKEN));
} /* end gll_is_special_symbol_token */


/* --------------------------------------------------------------------------
 * function gll_token_for_resword(lexeme, length)
 * --------------------------------------------------------------------------
 * Tests if the given lexeme represents a reserved word and returns the
 * corresponding token or TOKEN_UNKNOWN if it does not match a reserved word.
 * --------------------------------------------------------------------------
 */

gll_token_t gll_token_for_resword (const char *lexeme, uint_t length) {
  
  /* verify pre-conditions */
  if ((lexeme == NULL) || (length < 3) || (length > 8)) {
    return TOKEN_UNKNOWN;
  } /* end if */
  
  switch (length) {
    case /* length = */ 3 :
      /* EOF */
      if (str_match(lexeme, gll_resword_lexeme_table[TOKEN_EOF])) {
        return TOKEN_EOF;
      }
      
      else {
        return TOKEN_UNKNOWN;
      } /* end if */
    
    case /* length = */ 4 :
      /* endg */
      if (str_match(lexeme, gll_resword_lexeme_table[TOKEN_ENDG])) {
        return TOKEN_ENDG;
      }
      
      else {
        return TOKEN_UNKNOWN;
      } /* end if */
    
    case /* length = */ 5 :
      /* alias */
      if (str_match(lexeme, gll_resword_lexeme_table[TOKEN_ALIAS])) {
        return TOKEN_ALIAS;
      }
      
      else {
        return TOKEN_UNKNOWN;
      } /* end if */
    
    case /* length = */ 7 :
      /* grammar */
      if (str_match(lexeme, gll_resword_lexeme_table[TOKEN_GRAMMAR])) {
        return TOKEN_GRAMMAR;
      }
      
      else {
        return TOKEN_UNKNOWN;
      } /* end if */
    
    case /* length = */ 8 :
      /* reserved */
      if (str_match(lexeme, gll_resword_lexeme_table[TOKEN_RESERVED])) {
        return TOKEN_RESERVED;
      }
      
      else {
        return TOKEN_UNKNOWN;
      } /* end if */
    
    default :
      return TOKEN_UNKNOWN;
  } /* end switch */
  
} /* end gll_token_for_resword */


/* --------------------------------------------------------------------------
 * function gll_lexeme_for_resword(token)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to a NUL terminated character string with
 * the lexeme for the reserved word represented by token.  Returns NULL
 * if the token does not represent a reserved word.
 * --------------------------------------------------------------------------
 */

const char *gll_lexeme_for_resword (gll_token_t token) {
  uint_t index;
  
  /* check pre-conditions */
  if ((token < FIRST_RESERVED_WORD_TOKEN) ||
      (token > LAST_RESERVED_WORD_TOKEN)) {
    return NULL;
  } /* end if */
  
  index = (uint_t) token;
    
  /* return lexeme */
  return gll_resword_lexeme_table[index];
  
} /* end gll_lexeme_for_resword */


/* --------------------------------------------------------------------------
 * function gll_lexeme_for_special_symbol(token)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to a NUL terminated character string with
 * the lexeme for the special symbol represented by token.  Returns NULL
 * if the token does not represent a special symbol.
 * --------------------------------------------------------------------------
 */

const char *gll_lexeme_for_special_symbol (gll_token_t token) {
  uint_t index;
  
  /* check pre-conditions */
  if ((token < FIRST_SPECIAL_SYMBOL_TOKEN) ||
      (token > LAST_SPECIAL_SYMBOL_TOKEN)) {
    return NULL;
  } /* end if */
  
  index = token - FIRST_SPECIAL_SYMBOL_TOKEN;
  
  /* return lexeme */
  return gll_special_symbol_lexeme_table[index];
  
} /* end gll_lexeme_for_special_symbol */


/* --------------------------------------------------------------------------
 * function gll_name_for_token(token)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to a NUL terminated character string with
 * a human readable name for token.  Returns NULL if token is not valid.
 * --------------------------------------------------------------------------
 */

const char *gll_name_for_token (gll_token_t token) {
  if (token < TOKEN_END_MARK) {
    return gll_token_name_table[token];
  }
  else /* invalid token */ {
    return NULL;
  } /* end if */
} /* end gll_name_for_token */

/* END OF FILE */