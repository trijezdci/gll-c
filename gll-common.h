/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-common.h
 *
 * Common definitions.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_COMMON_H
#define GLL_COMMON_H

#include <stdint.h>


/* --------------------------------------------------------------------------
 * Common constants
 * ----------------------------------------------------------------------- */

#define ASCII_NUL 0

#define ASCII_CR 10

#define ASCII_LF 13

#define ASCII_TAB 8

#define ASCII_SPACE 32

#define EMPTY_STRING "\0"


/* --------------------------------------------------------------------------
 * Common macros
 * ----------------------------------------------------------------------- */

#define NOT(_expr) \
  (!(_expr))

#define CAST(_type,_var) \
  ((_type) _var)

#define SET_STATUS(_status_ptr,_value) \
  { if (_status_ptr != NULL) {*_status_ptr = _value; }; }


/* --------------------------------------------------------------------------
 * Common types
 * ----------------------------------------------------------------------- */

typedef unsigned int uint_t;

typedef unsigned char gll_char_t;


#endif /* GLL_COMMON_H */

/* END OF FILE */