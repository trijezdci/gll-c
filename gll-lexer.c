/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-lexer.h
 *
 * Implementation of GLL lexer module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gll-lexer.h"
#include "gll-common.h"
#include "gll-error.h"
#include "gll-filereader.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>


/* --------------------------------------------------------------------------
 * private type gll_symbol_struct_t
 * --------------------------------------------------------------------------
 * record type holding symbol details.
 * ----------------------------------------------------------------------- */

typedef struct {
  /* token */ gll_token_t token;
  /* line */ uint_t line;
  /* column */ uint_t column;
  /* lexeme */ gll_string_t lexeme;
} gll_symbol_struct_t;


/* --------------------------------------------------------------------------
 * null symbol for initialisation.
 * ----------------------------------------------------------------------- */

static const gll_symbol_struct_t null_symbol = {
  /* token */ TOKEN_UNKNOWN,
  /* line */ 0,
  /* column */ 0,
  /* lexeme */ NULL
}; /* null_symbol */


/* --------------------------------------------------------------------------
 * hidden type gll_lexer_struct_t
 * --------------------------------------------------------------------------
 * record type representing a Modula-2 lexer object.
 * ----------------------------------------------------------------------- */

struct gll_lexer_struct_t {
  /* infile */ gll_infile_t infile;
  /* current */ gll_symbol_struct_t current;
  /* lookahead */ gll_symbol_struct_t lookahead;
  /* status */ gll_lexer_status_t status;
  /* error_count */ uint_t error_count;
};

typedef struct gll_lexer_struct_t gll_lexer_struct_t;


/* --------------------------------------------------------------------------
 * Forward declarations
 * ----------------------------------------------------------------------- */

static void get_new_lookahead_sym (gll_lexer_t lexer);

static char skip_block_comment (gll_lexer_t lexer);

static char get_ident_or_resword (gll_lexer_t lexer, gll_token_t *token);

static char get_quoted_literal (gll_lexer_t lexer, gll_token_t *token);

static char get_char_code_literal (gll_lexer_t lexer, gll_token_t *token);


/* --------------------------------------------------------------------------
 * procedure gll_new_lexer(lexer, filename, status)
 * --------------------------------------------------------------------------
 * Allocates a new object of type gll_lexer_t, opens an input file and
 * associates the opened file with the newly created lexer object.
 *
 * pre-conditions:
 * o  parameter lexer must be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  pointer to newly allocated and opened lexer is passed back in lexer
 * o  GLL_LEXER_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if lexer is not NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * o  if the file represented by filename cannot be found
 *    status GLL_LEXER_STATUS_FILE_NOT_FOUND is returned
 * o  if the file represented by filename cannot be accessed
 *    status GLL_LEXER_STATUS_FILE_ACCESS_DENIED is returned
 * o  if no infile object could be allocated
 *    status GLL_LEXER_STATUS_ALLOCATION_FAILED is returned
 * ----------------------------------------------------------------------- */

void gll_new_lexer
  (gll_lexer_t *lexer, gll_string_t filename, gll_lexer_status_t *status) {
   
   gll_infile_t infile;
   gll_lexer_t new_lexer;
   gll_infile_status_t infile_status;
   
   /* check pre-conditions */
   if ((lexer == NULL) || (filename == NULL)) {
     SET_STATUS(status, GLL_LEXER_STATUS_INVALID_REFERENCE);
     return;
   } /* end if */
   
   new_lexer = malloc(sizeof(gll_lexer_struct_t));
   
   if (new_lexer == NULL) {
     SET_STATUS(status, GLL_LEXER_STATUS_ALLOCATION_FAILED);
     return;
   } /* end if */
   
   /* open source file */
   infile = gll_open_infile(filename, &infile_status);
   
   if (infile == NULL) {
     SET_STATUS(status, GLL_LEXER_STATUS_ALLOCATION_FAILED);
     free(new_lexer);
     return;
   } /* end if */
   
   /* initialise lexer object */
   new_lexer->infile = infile;
   new_lexer->current = null_symbol;
   new_lexer->lookahead = null_symbol;
   new_lexer->error_count = 0;
      
   /* read first symbol */
   get_new_lookahead_sym(new_lexer);
   
   *lexer = new_lexer;
   return;
} /* end gll_new_lexer */


/* --------------------------------------------------------------------------
 * function gll_read_sym(lexer)
 * --------------------------------------------------------------------------
 * Reads the lookahead symbol from the source file associated with lexer and
 * consumes it, thus advancing the current reading position, then returns
 * the symbol's token.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are updated
 * o  file status is set to GLL_LEXER_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * ----------------------------------------------------------------------- */

gll_token_t gll_read_sym (gll_lexer_t lexer) {
  
  /* release the lexeme of the current symbol */
  gll_string_release(lexer->current.lexeme);
  
  /* lookahead symbol becomes current symbol */
  lexer->current = lexer->lookahead;
  
  /* read new lookahead symbol */
  get_new_lookahead_sym(lexer);
  
  /* return current token */
  return lexer->current.token;
  
} /* end gll_read_sym */


/* --------------------------------------------------------------------------
 * function gll_next_sym(lexer)
 * --------------------------------------------------------------------------
 * Reads the lookahead symbol from the source file associated with lexer but
 * does not consume it, thus not advancing the current reading position,
 * then returns the symbol's token.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are NOT updated
 * o  file status is set to GLL_LEXER_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * ----------------------------------------------------------------------- */

inline gll_token_t gll_next_sym (gll_lexer_t lexer) {
  
  /* return lookahead token */
  return lexer->lookahead.token;

} /* end gll_next_sym */


/* --------------------------------------------------------------------------
 * function gll_lexer_filename(lexer)
 * --------------------------------------------------------------------------
 * Returns the filename associated with lexer.
 * ----------------------------------------------------------------------- */

gll_string_t gll_lexer_filename (gll_lexer_t lexer) {
  
  return gll_infile_filename(lexer->infile);
  
} /* end gll_lexer_filename */


/* --------------------------------------------------------------------------
 * function gll_lexer_status(lexer)
 * --------------------------------------------------------------------------
 * Returns the status of the last operation on lexer.
 * ----------------------------------------------------------------------- */

gll_lexer_status_t gll_lexer_status (gll_lexer_t lexer) {
  
  return lexer->status;
  
} /* end gll_lexer_status */


/* --------------------------------------------------------------------------
 * function gll_lexer_current_lexeme(lexer)
 * --------------------------------------------------------------------------
 * Returns the lexeme of the most recently consumed symbol.
 * ----------------------------------------------------------------------- */

gll_string_t gll_lexer_current_lexeme (gll_lexer_t lexer) {
  
  gll_string_retain(lexer->current.lexeme);
  
  return lexer->current.lexeme;
  
} /* end gll_lexer_current_lexeme */


/* --------------------------------------------------------------------------
 * function gll_lexer_current_line(lexer)
 * --------------------------------------------------------------------------
 * Returns the line counter of the most recently consumed symbol.
 * ----------------------------------------------------------------------- */

uint_t gll_lexer_current_line (gll_lexer_t lexer) {
  
  return lexer->current.line;
  
} /* end gll_lexer_current_line */


/* --------------------------------------------------------------------------
 * function gll_lexer_current_column(lexer)
 * --------------------------------------------------------------------------
 * Returns the column counter of the most recently consumed symbol.
 * ----------------------------------------------------------------------- */

uint_t gll_lexer_current_column (gll_lexer_t lexer) {
  
  return lexer->current.column;
  
} /* end gll_lexer_current_column */


/* --------------------------------------------------------------------------
 * procedure gll_release_lexer(lexer, status)
 * --------------------------------------------------------------------------
 * Closes the file associated with lexer, deallocates its file object,
 * deallocates the lexer object and returns NULL in lexer.
 *
 * pre-conditions:
 * o  parameter lexer must not be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  file object is deallocated
 * o  NULL is passed back in lexer
 * o  GLL_LEXER_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if lexer is NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * ----------------------------------------------------------------------- */

void gll_release_lexer (gll_lexer_t *lexptr, gll_lexer_status_t *status) {
  gll_lexer_t lexer;
  
  /* check pre-conditions */
  if ((lexptr == NULL) || (*lexptr == NULL)) {
    SET_STATUS(status, GLL_LEXER_STATUS_INVALID_REFERENCE);
    return;
  } /* end if */
  
  lexer = *lexptr;
  
  gll_close_infile(&lexer->infile, NULL);
  gll_string_release(lexer->current.lexeme);
  gll_string_release(lexer->lookahead.lexeme);
  
  free(lexer);
  *lexptr = NULL;
  return;
} /* end gll_release_lexer */


/* --------------------------------------------------------------------------
 * private function get_new_lookahead_sym(lexer)
 * ----------------------------------------------------------------------- */

static void get_new_lookahead_sym (gll_lexer_t lexer) {
  uint_t line, column;
  gll_token_t token;
  char next_char;
  
  /* no token yet */
  token = TOKEN_UNKNOWN;
  
  /* get the lookahead character */
  next_char = gll_next_char(lexer->infile);
  
  while (token == TOKEN_UNKNOWN) {
  
    /* skip all whitespace and line feeds */
    while ((next_char == ASCII_SPACE) ||
           (next_char == ASCII_TAB) ||
           (next_char == ASCII_LF)) {
      
      /* consume the character and get new lookahead */
      next_char = gll_consume_char(lexer->infile);
    } /* end while */
    
    /* get line and column of lookahead */
    line = gll_infile_current_line(lexer->infile);
    column = gll_infile_current_column(lexer->infile);
    
    switch (next_char) {
    
      case '\"' :
      case '\'' :
        /* string literal */
        next_char = get_quoted_literal(lexer, &token);
        if (token == TOKEN_MALFORMED_STRING) {
          gll_emit_error_w_pos
            (GLL_ERROR_MISSING_STRING_DELIMITER, line, column);
          lexer->error_count++;
        } /* end if */
        break;
        
      case '(' :
        /* left parenthesis */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_LEFT_PAREN;
        break;
        
      case ')' :
        /* right parenthesis */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_RIGHT_PAREN;
        break;
        
      case '*' :
        /* asterisk operator */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_ASTERISK;
        break;
        
      case '+' :
        /* plus operator */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_PLUS;
        break;
        
      case ',' :
        /* comma */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_COMMA;
        break;
                
      case '.' :
        /* range or period */
        next_char = gll_consume_char(lexer->infile);
        if /* range */ (next_char == '.') {
          next_char = gll_consume_char(lexer->infile);
          token = TOKEN_RANGE;
        }
        else /* period */ {
          token = TOKEN_PERIOD;
        } /* end if */
        break;
        
      case '/' :
        if /* block comment */ (gll_la2_char(lexer->infile) == '*') {
          next_char = skip_block_comment(lexer);
          token = TOKEN_UNKNOWN;
        }
        else /* invalid char */ {
          gll_emit_error_w_chr
            (GLL_ERROR_INVALID_INPUT_CHAR, line, column, next_char);
          next_char = gll_consume_char(lexer->infile);
          lexer->error_count++;
          token = TOKEN_UNKNOWN;
        } /* end if */
        break;
        
      case '0' :
        /* character code literal */
        if (gll_la2_char(lexer->infile) == 'u') {
          next_char = get_char_code_literal(lexer, &token);
        }
        else /* invalid char */ {
          gll_emit_error_w_chr
            (GLL_ERROR_INVALID_INPUT_CHAR, line, column, next_char);
          next_char = gll_consume_char(lexer->infile);
          lexer->error_count++;
          token = TOKEN_UNKNOWN;
        } /* end if */
        break;
        
      case ':' :
        if /* colon-equal */ (gll_la2_char(lexer->infile) == '=') {
          next_char = gll_consume_char(lexer->infile);
          next_char = gll_consume_char(lexer->infile);
          token = TOKEN_COLONEQUAL;
        }
        else /* invalid char */ {
          gll_emit_error_w_chr
            (GLL_ERROR_INVALID_INPUT_CHAR, line, column, next_char);
          next_char = gll_consume_char(lexer->infile);
          lexer->error_count++;
          token = TOKEN_UNKNOWN;
        } /* end if */
        break;
        
      case ';' :
        /* semicolon */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_SEMICOLON;
        break;
        
      case '=' :
        /* equal operator */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_EQUAL;
        break;
        
      case '?' :
        /* option operator */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_QMARK;
        break;
        
      case 'A' :
      case 'B' :
      case 'C' :
      case 'D' :
      case 'E' :
      case 'F' :
      case 'G' :
      case 'H' :
      case 'I' :
      case 'J' :
      case 'K' :
      case 'L' :
      case 'M' :
      case 'N' :
      case 'O' :
      case 'P' :
      case 'Q' :
      case 'R' :
      case 'S' :
      case 'T' :
      case 'U' :
      case 'V' :
      case 'W' :
      case 'X' :
      case 'Y' :
      case 'Z' :
        
      case 'a' :
      case 'b' :
      case 'c' :
      case 'd' :
      case 'e' :
      case 'f' :
      case 'g' :
      case 'h' :
      case 'i' :
      case 'j' :
      case 'k' :
      case 'l' :
      case 'm' :
      case 'n' :
      case 'o' :
      case 'p' :
      case 'q' :
      case 'r' :
      case 's' :
      case 't' :
      case 'u' :
      case 'v' :
      case 'w' :
      case 'x' :
      case 'y' :
      case 'z' :
        /* identifier or reserved word */
        next_char = get_ident_or_resword(lexer, &token);
        break;
        
      case '|' :
        /* vertical bar */
        next_char = gll_consume_char(lexer->infile);
        token = TOKEN_BAR;
        break;
        
      case EOF :
        token = TOKEN_END_OF_FILE;
        break;
        
      default :
        /* invalid character */
        gll_emit_error_w_chr
          (GLL_ERROR_INVALID_INPUT_CHAR, line, column, next_char);
        next_char = gll_consume_char(lexer->infile);
        lexer->error_count++;
        token = TOKEN_UNKNOWN;
    } /* end switch */
  } /* end while */
  
  /* update lexer's lookahead symbol */
  lexer->lookahead.token = token;
  lexer->lookahead.line = line;
  lexer->lookahead.column = column;
  return;
} /* end get_new_lookahead_sym */


/* --------------------------------------------------------------------------
 * private function skip_block_comment(lexer)
 * ----------------------------------------------------------------------- */

static char skip_block_comment(gll_lexer_t lexer) {
  bool end_of_comment_reached = false;
  char next_char;
  
  /* consume opening '/' */
  next_char = gll_consume_char(lexer->infile);

  /* consume opening '*' */
  next_char = gll_consume_char(lexer->infile);
  
  while (!end_of_comment_reached) {
  
    /* check for end of comment */
    if (next_char == '*') {
      next_char = gll_consume_char(lexer->infile);
      if (next_char == '/') {
        next_char = gll_consume_char(lexer->infile);
        end_of_comment_reached = true;
      } /* end if */
    }
    
    /* check for any other character */
    else if (next_char != EOF) {
      next_char = gll_consume_char(lexer->infile);
    }
    
    else /* next_char == EOF */ {
      gll_emit_error(GLL_ERROR_EOF_IN_BLOCK_COMMENT);
      lexer->error_count++;
    } /* end if */
  } /* end while */
  
  return next_char;
} /* end skip_block_comment */


/* --------------------------------------------------------------------------
 * function macro IS_LOWER()
 * --------------------------------------------------------------------------
 * Tests if its argument represents a lowercase letter.
 * ----------------------------------------------------------------------- */

#define IS_LOWER(_ch) \
  (((_ch) >= 'a') && ((_ch) <= 'z'))


/* --------------------------------------------------------------------------
 * function macro IS_UPPER()
 * --------------------------------------------------------------------------
 * Tests if its argument represents an uppercase letter.
 * ----------------------------------------------------------------------- */

#define IS_UPPER(_ch) \
  (((_ch) >= 'A') && ((_ch) <= 'Z'))


/* --------------------------------------------------------------------------
 * function macro IS_DIGIT()
 * --------------------------------------------------------------------------
 * Tests if its argument represents a decimal digit.
 * ----------------------------------------------------------------------- */

#define IS_DIGIT(_ch) \
  (((_ch) >= '0') && ((_ch) <= '9'))


/* --------------------------------------------------------------------------
 * function macro IS_ASCII_ALNUM()
 * --------------------------------------------------------------------------
 * Tests if its argument represents a letter or digit.
 * ----------------------------------------------------------------------- */

#define IS_ALNUM(_ch) \
  (IS_LOWER(_ch) || IS_UPPER(_ch) || IS_DIGIT(_ch))


/* --------------------------------------------------------------------------
 * function macro IS_A_TO_F()
 * --------------------------------------------------------------------------
 * Tests if its argument represents any of 'A', 'B', 'C', 'D', 'E' or 'F'.
 * ----------------------------------------------------------------------- */

#define IS_A_TO_F(_ch) \
  (((_ch) >= 'A') && ((_ch) <= 'F'))


/* --------------------------------------------------------------------------
 * private function get_ident_or_resword(lexer)
 * ----------------------------------------------------------------------- */

static char get_ident_or_resword(gll_lexer_t lexer, gll_token_t *token) {
  gll_token_t intermediate_token;
  char next_char, la2_char;
  bool lowercase;
  
  gll_mark_lexeme(lexer->infile);
  next_char = gll_next_char(lexer->infile);
  la2_char = gll_la2_char(lexer->infile);
  
  /* remember case of first character */
  lowercase = IS_LOWER(next_char);
  
  while (IS_ALNUM(next_char) ||
         ((next_char == '_') && (IS_ALNUM(la2_char)))) {
    next_char = gll_consume_char(lexer->infile);
    la2_char = gll_la2_char(lexer->infile);
  } /* end while */
  
  /* get lexeme */
  lexer->lookahead.lexeme = gll_read_marked_lexeme(lexer->infile);
  
  /* check if lexeme is reserved word */
  intermediate_token = gll_token_for_resword
    (gll_string_char_ptr(lexer->lookahead.lexeme),
     gll_string_length(lexer->lookahead.lexeme));
  
  /* determine reserved word, lower- or uppercase identifier */
  if (intermediate_token != TOKEN_UNKNOWN) {
    *token = intermediate_token;
  }
  else if (lowercase) {
    *token = TOKEN_LOWER_IDENT;
  }
  else /* uppercase */ {
    *token = TOKEN_UPPER_IDENT;
  } /* end if */
  
  return next_char;
} /* end get_ident_or_resword */


/* --------------------------------------------------------------------------
 * private function get_quoted_literal(lexer)
 * ----------------------------------------------------------------------- */

static char get_quoted_literal(gll_lexer_t lexer, gll_token_t *token) {
  char first_char, next_char, quotation_mark;
  bool missing_delimiter = false;
  uint_t length = 0;
    
  /* consume opening quotation mark */
  quotation_mark = gll_read_char(lexer->infile);
  
  gll_mark_lexeme(lexer->infile);
  first_char = gll_next_char(lexer->infile);
  next_char = first_char;
  
  while (next_char != quotation_mark) {
    
    if (next_char == EOF || next_char == '\n') {
      missing_delimiter = true;
      break;
    } /* end if */
    
    next_char = gll_consume_char(lexer->infile);
    length++;
  } /* end while */
  
  /* get lexeme */
  lexer->lookahead.lexeme = gll_read_marked_lexeme(lexer->infile);
  
  /* consume closing quotation mark */
  if (next_char == quotation_mark) {
    next_char = gll_consume_char(lexer->infile);
  } /* end if */
  
  /* pass back token */
  if ((missing_delimiter) || (length == 0)) {
    *token = TOKEN_MALFORMED_LITERAL;
  }
  else if (length == 1) {
    if (IS_LOWER(first_char)) {
      *token = TOKEN_QUOTED_LOWER;
    }
    else if (IS_UPPER(first_char)) {
      *token = TOKEN_QUOTED_UPPER;
    }
    else if (IS_DIGIT(first_char)) {
      *token = TOKEN_QUOTED_DIGIT;
    }
    else /* non-alphanumeric */ {
      *token = TOKEN_QUOTED_NONALNUM;
    } /* end if */
  }
  else {
    *token = TOKEN_STRING;
  } /* end if */
  
  return next_char;
} /* end get_quoted_literal */


/* --------------------------------------------------------------------------
 * private function get_char_code_literal(lexer, token)
 * ----------------------------------------------------------------------- */

static char get_char_code_literal
  (gll_lexer_t lexer, gll_token_t *token) {
  
  gll_token_t intermediate_token;
  uint_t digit_count = 0;
  char next_char;
  
  gll_mark_lexeme(lexer->infile);
  
  /* consume '0' */
  next_char = gll_consume_char(lexer->infile);
     
  /* consume 'u' */
  next_char = gll_consume_char(lexer->infile);
  
  /* consume all digits */
  while (IS_DIGIT(next_char) || IS_A_TO_F(next_char)) {
    next_char = gll_consume_char(lexer->infile);
    digit_count++;
  } /* end while */
    
  /* get lexeme */
  lexer->lookahead.lexeme = gll_read_marked_lexeme(lexer->infile);
  
  /* pass back token */
  if (digit_count == 0) {
    *token = TOKEN_MALFORMED_LITERAL;
  }
  else {
    *token = TOKEN_CHAR_CODE;
  } /* end if */
  
  return next_char;
} /* end get_char_code_literal */

/* END OF FILE */