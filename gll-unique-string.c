/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-unique-string.h
 *
 * Implementation of GLL unique string module.
 *
 * The module provides a dynamic string ADT managed in an internal
 * global string repository to avoid duplicate string allocation.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gll-unique-string.h"

#include "hash.h"

#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>


/* --------------------------------------------------------------------------
 * Defaults
 * ----------------------------------------------------------------------- */

#define GLL_STRING_REPO_DEFAULT_BUCKET_COUNT 2011


/* --------------------------------------------------------------------------
 * private type gll_hash_t
 * --------------------------------------------------------------------------
 * unsigned integer type representing a 32-bit hash value.
 * ----------------------------------------------------------------------- */

typedef uint32_t gll_hash_t;


/* --------------------------------------------------------------------------
 * hidden type gll_string_struct_t
 * --------------------------------------------------------------------------
 * record type representing a dynamic string object.
 * ----------------------------------------------------------------------- */

struct gll_string_struct_t {
  /* ext_char_ptr */ char *ext_char_ptr;
  /* ref_count */ uint_t ref_count;
  /* length */ uint_t length;
  /* char_array */ char char_array[];
};

typedef struct gll_string_struct_t gll_string_struct_t;


/* --------------------------------------------------------------------------
 * private types gll_string_repo_entry_t and gll_string_repo_entry_s
 * --------------------------------------------------------------------------
 * pointer and record type representing a string repository entry.
 * ----------------------------------------------------------------------- */

typedef struct gll_string_repo_entry_s *gll_string_repo_entry_t;

struct gll_string_repo_entry_s {
  /* key */ gll_hash_t key;
  /* str */ gll_string_t str;
  /* next */ gll_string_repo_entry_t next;
};

typedef struct gll_string_repo_entry_s gll_string_repo_entry_s;


/* --------------------------------------------------------------------------
 * private types gll_string_repo_t and gll_string_repo_s
 * --------------------------------------------------------------------------
 * pointer and record type representing the string repository.
 * ----------------------------------------------------------------------- */

typedef struct gll_string_repo_s *gll_string_repo_t;

struct gll_string_repo_s {
  /* entry_count */ uint_t entry_count;
  /* bucket_count */ uint_t bucket_count;
  /* bucket */ gll_string_repo_entry_t bucket[];
};

typedef struct gll_string_repo_s gll_string_repo_s;


/* --------------------------------------------------------------------------
 * private variable repository
 * --------------------------------------------------------------------------
 * pointer to global string repository.
 * ----------------------------------------------------------------------- */

static gll_string_repo_t repository = NULL;


/* --------------------------------------------------------------------------
 * Forward declarations
 * ----------------------------------------------------------------------- */

void gll_init_string_repository (uint_t size, gll_string_status_t *status);

bool matches_str_and_length
  (gll_string_t str, const char *cmpstr, uint_t length);

gll_string_t new_string_from_string (char *str, uint_t length);

gll_string_t new_string_from_slice (char *str, uint_t offset, uint_t length);

gll_string_t new_string_from_concatenation
  (char *str, char *append_str, uint_t str_len, uint_t append_str_len);

gll_string_repo_entry_t new_repo_entry (gll_string_t str, gll_hash_t key);


/* --------------------------------------------------------------------------
 * private macros
 * ----------------------------------------------------------------------- */

/* NB: Not EBCDIC compatible */
#define IS_CONTROL_CHAR(_ch) \
  ((_ch < 32) || (_ch > 126))


/* --------------------------------------------------------------------------
 * procedure gll_init_string_repository(size, status)
 * --------------------------------------------------------------------------
 * Allocates and initialises global string repository.  Parameter size
 * determines the number of buckets of the repository's internal hash table.
 * If size is zero, value GLL_STRING_REPO_DEFAULT_BUCKET_COUNT is used.
 *
 * pre-conditions:
 * o  global repository must be uninitialised upon entry
 * o  parameter size may be zero upon entry
 * o  parameter status may be NULL upon entry
 *
 * post-conditions:
 * o  global string repository is allocated and intialised.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if repository has already been initialised upon entry,
 *    no operation is carried out and GLL_STRING_STATUS_ALREADY_INITIALIZED
 *    is passed back in status unless status is NULL
 * o  if repository allocation failed, GLL_STRING_STATUS_ALLOCATION_FAILED
 *    is passed back in status unless status is NULL
 * --------------------------------------------------------------------------
 */

void gll_init_string_repository
  (uint_t size, gll_string_status_t *status) {
  
  uint_t index, bucket_count, allocation_size;
  
  /* check pre-conditions */
  if (repository != NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_ALREADY_INITIALIZED);
    return;
  } /* end if */
  
  /* determine bucket count */
  if (size == 0) {
    bucket_count = GLL_STRING_REPO_DEFAULT_BUCKET_COUNT;
  }
  else /* size != 0 */ {
    bucket_count = size;
  } /* end if */
  
  /* allocate repository */
  allocation_size = sizeof(gll_string_repo_s) +
    bucket_count * sizeof(gll_string_repo_entry_t);
  repository = malloc(allocation_size);
  
  /* bail out if allocation failed */
  if (repository == NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_ALLOCATION_FAILED);
    return;
  } /* end if */
  
  /* set entry and bucket count */
  repository->entry_count = 0;
  repository->bucket_count = bucket_count;
  
  /* initialise buckets */
  index = 0;
  while (index < bucket_count) {
    repository->bucket[index] = NULL;
    index++;
  } /* end while */
  
  SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
  return;
} /* end gll_init_string_repository */


/* --------------------------------------------------------------------------
 * function gll_get_string(str, status)
 * --------------------------------------------------------------------------
 * Returns a unique string object for str which must be a pointer to a NUL
 * terminated character string.  
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  if a string object for str is present in the internal repository,
 *    that string object is retrieved, retained and returned.
 * o  if no string object for str is present in the repository,
 *    a new string object with a copy of str is created, stored and returned.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if str is NULL upon entry, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_INVALID_REFERENCE is
 *    passed back in status unless status is NULL
 * o  if string object allocation failed, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_ALLOCATION_FAILED is
 *    passed back in status unless status is NULL
 * --------------------------------------------------------------------------
 */

gll_string_t gll_get_string (char *str, gll_string_status_t *status) {
  gll_string_repo_entry_t new_entry, this_entry;
  gll_string_t new_string, this_string;
  uint_t index, length;
  gll_hash_t key;
  char ch;
  
  /* check repository */
  if (repository == NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_NOT_INITIALIZED);
    return NULL;
  } /* end if */
  
  /* check str */
  if (str == NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_INVALID_REFERENCE);
    return NULL;
  } /* end if */
    
  /* determine length and key */
  index = 0;
  ch = str[index];
  key = HASH_INITIAL;
  while (ch != ASCII_NUL) {
    key = HASH_NEXT_CHAR(key, ch);
    index++;
    ch = str[index];
  } /* end while */
  
  length = index + 1;
  key = HASH_FINAL(key);
  
  /* determine bucket index */
  index = key % repository->bucket_count;
  
  /* check if str is already in repository */
  if /* bucket empty */ (repository->bucket[index] == NULL) {
    
    /* create a new string object */
    new_string = new_string_from_string(str, length);
    
    /* create a new repository entry */
    new_entry = new_repo_entry(new_string, key);
    
    /* link the bucket to the new entry */
    repository->bucket[index] = new_entry;
    
    /* update the entry counter */
    repository->entry_count++;
    
    /* set status and return string object */
    SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
    return new_string;
  }
  else /* bucket not empty */ {
    
    /* first entry in bucket is starting point */
    this_entry = repository->bucket[index];
    
    /* search bucket for matching string */
    while (true) {
      if /* match found in current entry */
        ((this_entry->key == key) &&
          matches_str_and_length(this_entry->str, str, length)) {
        
        /* get string object of matching entry and retain it */
        this_string = this_entry->str;
        gll_string_retain(this_string);
        
        /* set status and return string object */
        SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
        return this_string;
      }
      else if /* last entry reached without match */
        (this_entry->next == NULL) {
        
        /* create a new string object */
        new_string = new_string_from_string(str, length);
        
        /* create a new repository entry */
        new_entry = new_repo_entry(new_string, key);
        
        /* link last entry to the new entry */
        this_entry->next = new_entry;
        
        /* update the entry counter */
        repository->entry_count++;
        
        /* set status and return string object */
        SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
        return new_string;
      }
      else /* not last entry yet, move to next entry */ {
        this_entry = this_entry->next;
      } /* end if */
    } /* end while */      
  } /* end if */  
} /* end gll_get_string */


/* --------------------------------------------------------------------------
 * function gll_get_string_for_slice(str, offset, length, status)
 * --------------------------------------------------------------------------
 * Returns a unique string object for a given slice of str.  Parameter str
 * must be a pointer to a NUL terminated character string.  The position of
 * the first character of the slice is given by parameter offset and the
 * length of the slice is given by parameter length.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 * o  slice must be within range of str (NOT GUARDED),
 * o  slice must not contain any characters with code points < 32 or > 126 
 * 
 *
 * post-conditions:
 * o  if a string object for the slice is present in the internal repository,
 *    that string object is retrieved, retained and returned.
 * o  if no string object for the slice is present in the repository, a new
 *    string object with a copy of the slice is created, stored and returned.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if precondition #1 is not met, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_INVALID_REFERENCE is
 *    passed back in status, unless status is NULL
 * o  if precondition #3 is not met, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_INVALID_INDICES is
 *    passed back in status, unless status is NULL
 * o  if string object allocation failed, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_ALLOCATION_FAILED is
 *    passed back in status unless status is NULL
 * --------------------------------------------------------------------------
 */

gll_string_t gll_get_string_for_slice
  (char *str, uint_t offset, uint_t length, gll_string_status_t *status) {
  
  gll_string_repo_entry_t new_entry, this_entry;
  gll_string_t new_string, this_string;
  uint_t index;
  gll_hash_t key;
  char ch;
  
  /* check repository */
  if (repository == NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_NOT_INITIALIZED);
    return NULL;
  } /* end if */
  
  /* check str */
  if (str == NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_INVALID_REFERENCE);
    return NULL;
  } /* end if */
  
  /* determine key for slice */
  index = offset;
  key = HASH_INITIAL;
  while (index < length) {
    ch = str[index];
    /* bail out if any control codes are found */
    if (IS_CONTROL_CHAR(ch)) {
      SET_STATUS(status, GLL_STRING_STATUS_INVALID_INDICES);
      return NULL;
    } /* end if */
    key = HASH_NEXT_CHAR(key, ch);
    index++;
  } /* end while */
  
  key = HASH_FINAL(key);
  
  /* determine bucket index */
  index = key % repository->bucket_count;
  
  /* check if str is already in repository */
  if /* bucket empty */ (repository->bucket[index] == NULL) {
    
    /* create a new string object */
    new_string = new_string_from_slice(str, offset, length);
    
    /* create a new repository entry */
    new_entry = new_repo_entry(new_string, key);
    
    /* link the bucket to the new entry */
    repository->bucket[index] = new_entry;
    
    /* update the entry counter */
    repository->entry_count++;
    
    /* set status and return string object */
    SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
    return new_string;
  }
  else /* bucket not empty */ {
    
    /* first entry in bucket is starting point */
    this_entry = repository->bucket[index];
    
    /* search bucket for matching string */
    while (true) {
      if /* match found in current entry */
        ((this_entry->key == key) &&
          matches_str_and_length(this_entry->str, str, length)) {
        
        /* get string object of matching entry and retain it */
        this_string = this_entry->str;
        gll_string_retain(this_string);
        
        /* set status and return string object */
        SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
        return this_string;
      }
      else if /* last entry reached without match */
        (this_entry->next == NULL) {
        
        /* create a new string object */
        new_string = new_string_from_slice(str, offset, length);
        
        /* create a new repository entry */
        new_entry = new_repo_entry(new_string, key);
        
        /* link last entry to the new entry */
        this_entry->next = new_entry;
        
        /* update the entry counter */
        repository->entry_count++;
        
        /* set status and return string object */
        SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
        return new_string;
      }
      else /* not last entry yet, move to next entry */ {
        this_entry = this_entry->next;
      } /* end if */
    } /* end while */      
  } /* end if */  
} /* end gll_get_string_for_slice */


/* --------------------------------------------------------------------------
 * function gll_get_string_for_concatenation(str, append_str, status)
 * --------------------------------------------------------------------------
 * Allocates a new object of type gll_string_t and initialises it with a
 * copy of str appended by a copy of append_str each of which must be a
 * pointer to a NUL terminated character string.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 * o  parameter append_str must not be NULL upon entry
 *
 * post-conditions:
 * o  pointer to newly allocated dynamic string is returned
 * o  copy of character string and its length is stored internally
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if any of str or append_str is NULL upon entry, no operation is
 *    carried out, NULL is returned and GLL_STRING_STATUS_INVALID_REFERENCE
 *    is passed back in status, unless NULL
 * o  if no dynamic string object could be allocated, NULL is returned,
 *    and GLL_STRING_STATUS_ALLOCATION_FAILED is passed back in status,
 *    unless NULL
 * --------------------------------------------------------------------------
 */

gll_string_t gll_get_string_for_concatenation
  (char *str, char *append_str, gll_string_status_t *status) {
  
  gll_string_repo_entry_t new_entry, this_entry;
  gll_string_t new_string, this_string;
  uint_t index, length, str_len, append_str_len;
  gll_hash_t key;
  char ch;
  
  /* check repository */
  if (repository == NULL) {
    SET_STATUS(status, GLL_STRING_STATUS_NOT_INITIALIZED);
    return NULL;
  } /* end if */
  
  /* check str and append_str */
  if ((str == NULL) || (append_str == NULL)) {
    SET_STATUS(status, GLL_STRING_STATUS_INVALID_REFERENCE);
    return NULL;
  } /* end if */
  
  /* determine length of str */
  index = 0;
  ch = str[index];
  key = HASH_INITIAL;
  while (ch != ASCII_NUL) {
    key = HASH_NEXT_CHAR(key, ch);
    index++;
    ch = str[index];
  } /* end while */
  
  str_len = index + 1;
  
  /* determine length of append_str */
  index = 0;
  ch = append_str[index];
  key = HASH_INITIAL;
  while (ch != ASCII_NUL) {
    key = HASH_NEXT_CHAR(key, ch);
    index++;
    ch = append_str[index];
  } /* end while */
  
  append_str_len = index + 1;
  
  /* finalise length and key of concatenation */
  length = str_len + append_str_len;
  key = HASH_FINAL(key);
  
  /* determine bucket index */
  index = key % repository->bucket_count;
  
  /* check if str is already in repository */
  if /* bucket empty */ (repository->bucket[index] == NULL) {
    
    /* create a new string object */
    new_string =
      new_string_from_concatenation(str, append_str, str_len, append_str_len);
    
    /* create a new repository entry */
    new_entry = new_repo_entry(new_string, key);
    
    /* link the bucket to the new entry */
    repository->bucket[index] = new_entry;
    
    /* update the entry counter */
    repository->entry_count++;
    
    /* set status and return string object */
    SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
    return new_string;
  }
  else /* bucket not empty */ {
    
    /* first entry in bucket is starting point */
    this_entry = repository->bucket[index];
    
    /* search bucket for matching string */
    while (true) {
      if /* match found in current entry */
        ((this_entry->key == key) &&
          matches_str_and_length(this_entry->str, str, length)) {
        
        /* get string object of matching entry and retain it */
        this_string = this_entry->str;
        gll_string_retain(this_string);
        
        /* set status and return string object */
        SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
        return this_string;
      }
      else if /* last entry reached without match */
        (this_entry->next == NULL) {
        
        /* create a new string object */
        new_string =
          new_string_from_concatenation
            (str, append_str, str_len, append_str_len);
        
        /* create a new repository entry */
        new_entry = new_repo_entry(new_string, key);
        
        /* link last entry to the new entry */
        this_entry->next = new_entry;
        
        /* update the entry counter */
        repository->entry_count++;
        
        /* set status and return string object */
        SET_STATUS(status, GLL_STRING_STATUS_SUCCESS);
        return new_string;
      }
      else /* not last entry yet, move to next entry */ {
        this_entry = this_entry->next;
      } /* end if */
    } /* end while */      
  } /* end if */  
} /* end gll_new_string_by_appending_string */


/* --------------------------------------------------------------------------
 * function gll_string_length(str)
 * --------------------------------------------------------------------------
 * Returns the length of str.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  the length of str is returned
 *
 * error-conditions:
 * o  if str is NULL upon entry, zero is returned
 * --------------------------------------------------------------------------
 */

inline uint_t gll_string_length (gll_string_t str) {
  uint_t length;
  
  if (str == NULL) {
    length = 0;
  }
  else {
    length = str->length;
  } /* end if */
  
  return length;
} /* end gll_string_length */


/* --------------------------------------------------------------------------
 * function gll_string_char_ptr(str)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to str's character string.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  an immutable pointer to the character string of str is returned
 *
 * error-conditions:
 * o  if str is NULL upon entry, NULL is returned
 * --------------------------------------------------------------------------
 */

inline const char *gll_string_char_ptr (gll_string_t str) {
  const char *char_ptr;
  
  if /* invalid string */ (str == NULL) {
    char_ptr = NULL;
  }
  else if /* external reference */ (str->ext_char_ptr != NULL) {
    char_ptr = (const char *) str->ext_char_ptr;
  }
  else /* internal character string */ {
    char_ptr = (const char *) &(str->char_array);
  } /* end if */
  
  return char_ptr;
} /* end gll_string_char_ptr */


/* --------------------------------------------------------------------------
 * function gll_unique_string_count()
 * --------------------------------------------------------------------------
 * Returns the number of unique strings stored in the string repository.
 *
 * pre-conditions:
 * o  none
 *
 * post-conditions:
 * o  none
 *
 * error-conditions:
 * o  none
 * --------------------------------------------------------------------------
 */

inline uint_t gll_unique_string_count (void) {
  
  if (repository != NULL) {
    return repository->entry_count;
  }
  else /* repository == NULL */ {
    return 0;
  } /* end if */
  
} /* end gll_unique_string_count */


/* --------------------------------------------------------------------------
 * function gll_string_retain(str)
 * --------------------------------------------------------------------------
 * Prevents str from deallocation.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  str's reference count is incremented.
 *
 * error-conditions:
 * o  if str is not NULL upon entry, no operation is carried out
 * --------------------------------------------------------------------------
 */

inline void gll_string_retain (gll_string_t str) {
  
  if /* valid string */ (str != NULL) {
    str->ref_count++;
  } /* end if */
  
} /* end gll_string_retain */


/* --------------------------------------------------------------------------
 * function gll_string_release(str)
 * --------------------------------------------------------------------------
 * Cancels an outstanding retain, or deallocates str if there are no
 * outstanding retains.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  if str's reference count is zero upon entry, str is deallocated
 * o  if str's reference count is not zero upon entry, it is decremented
 *
 * error-conditions:
 * o  if str is not NULL upon entry, no operation is carried out
 * --------------------------------------------------------------------------
 */

void gll_string_release (gll_string_t str) {
  
  if /* valid string */ (str != NULL) {
    if (str->ref_count > 1) {
      /* decrement reference counter */
      str->ref_count--;
    }
    else {
      /* deallocate */
      free(str);
    } /* end if */
  } /* end if */
  
} /* end gll_string_release */


/* --------------------------------------------------------------------------
 * private function new_string_from_string(str, length)
 * --------------------------------------------------------------------------
 * Allocates and returns a new string object, initialised with str and
 * length.  Returns NULL if allocation failed.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry (NOT GUARDED)
 *
 * post-conditions:
 * o  newly allocated, initalised and retained string object is returned
 *
 * error-conditions:
 * o  if allocation fails, no operation is carried out, NULL is returned
 * --------------------------------------------------------------------------
 */

gll_string_t new_string_from_string (char *str, uint_t length) {
  gll_string_t new_string;
  uint_t index;
  
  /* allocate new string object */
  new_string = malloc(sizeof(gll_string_struct_t) + length + 1);
  
  /* check allocation, bail out on failure */
  if /* allocation failed */ (new_string == NULL) {
    return NULL;
  } /* end if */
    
  /* initialise */
  new_string->ref_count = 1;
  new_string->length = length;
  new_string->ext_char_ptr = NULL;
  
  /* copy string */
  index = 0;
  while (index < length) {
    new_string->char_array[index] = str[index];
    index++;
  } /* end while */
  
  /* terminate character array */
  new_string->char_array[index] = ASCII_NUL;
  
  return new_string;
} /* end gll_new_string */


/* --------------------------------------------------------------------------
 * private function match_str_of_length(str1, str2, length)
 * --------------------------------------------------------------------------
 * Compares character strings str1 and str2 up to length characters
 * and returns true if they match, otherwise false.
 *
 * pre-conditions:
 * o  parameters str1 and str2 must not be NULL upon entry (NOT GUARDED)
 *
 * post-conditions:
 * o  boolean result is returned
 *
 * error-conditions:
 * o  none
 * --------------------------------------------------------------------------
 */

bool matches_str_and_length
  (gll_string_t str, const char *cmpstr, uint_t length) {
  
  uint_t index;
  
  if (str->length != length) {
    return false;
  } /* end if */
  
  index = 0;
  while (index < length + 1) {
    if (str->char_array[index] != cmpstr[index]) {
      return false;
    }
    else if (str->char_array[index] == '\0') {
      return true;
    }
    else {
      index++;
    } /* end if */
  } /* end while */
  return false;
} /* end matches_str_and_length */


/* --------------------------------------------------------------------------
 * private function new_string_from_slice(str, offset, length)
 * --------------------------------------------------------------------------
 * Allocates and returns a new string object.  The string is initialised with
 * a slice of str starting at offset and ending at offset+length.
 * Returns NULL if allocation failed.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry (NOT GUARDED)
 *
 * post-conditions:
 * o  newly allocated, initalised and retained string object is returned
 *
 * error-conditions:
 * o  if allocation fails, no operation is carried out, NULL is returned
 * --------------------------------------------------------------------------
 */

gll_string_t new_string_from_slice
  (char *str, uint_t offset, uint_t length) {
  
  gll_string_t new_string;
  uint_t source_index, target_index;
  
  /* allocate new string object */
  new_string = malloc(sizeof(gll_string_struct_t) + length + 1);
  
  /* check allocation, bail out on failure */
  if /* allocation failed */ (new_string == NULL) {
    return NULL;
  } /* end if */
    
  /* initialise */
  new_string->ref_count = 1;
  new_string->length = length;
  new_string->ext_char_ptr = NULL;
  
  /* copy slice */
  target_index = 0;
  source_index = offset;
  while (target_index < length) {
    new_string->char_array[target_index] = str[source_index];
    target_index++;
    source_index++;
  } /* end while */
  
  /* terminate character array */
  new_string->char_array[target_index] = ASCII_NUL;
  
  return new_string;
} /* end gll_new_string_from_slice */


/* --------------------------------------------------------------------------
 * private function new_string_from_concatenation(str, append_str, len, len2)
 * --------------------------------------------------------------------------
 * Allocates and returns a new string object.  The string is initialised with
 * the concatenation product of str and append_str.
 * Returns NULL if allocation failed.
 *
 * pre-conditions:
 * o  parameters str and append_str must not be NULL upon entry (NOT GUARDED)
 *
 * post-conditions:
 * o  newly allocated, initalised and retained string object is returned
 *
 * error-conditions:
 * o  if allocation fails, no operation is carried out, NULL is returned
 * --------------------------------------------------------------------------
 */

gll_string_t new_string_from_concatenation
  (char *str, char *append_str, uint_t str_len, uint_t append_str_len) {
  
  gll_string_t new_string;
  uint_t source_index, target_index, length;
  
  length = str_len + append_str_len;
  
  /* allocate new dynamic string */
  new_string = malloc(sizeof(gll_string_struct_t) + length + 1);
  
  /* check allocation, bail out on failure */
  if /* allocation failed */ (new_string == NULL) {
    return NULL;
  } /* end if */
      
  /* initialise */
  new_string->ref_count = 1;
  new_string->length = length;
  new_string->ext_char_ptr = NULL;
  
  target_index = 0;
  
  /* copy str */
  source_index = 0;
  while (target_index < str_len) {
    new_string->char_array[target_index] = str[source_index];
    target_index++;
    source_index++;
  } /* end while */
  
  /* copy append_str */
  source_index = 0;
  while (target_index < append_str_len) {
    new_string->char_array[target_index] = append_str[source_index];
    target_index++;
    source_index++;
  } /* end while */
  
  /* terminate character array */
  new_string->char_array[target_index] = ASCII_NUL;
  
  return new_string;
} /* end new_string_from_concatenation */


/* --------------------------------------------------------------------------
 * private function new_repo_entry(str, key)
 * --------------------------------------------------------------------------
 * Allocates and returns a new repository entry, initialised with str and
 * key.  Returns NULL if allocation failed.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry (NOT GUARDED)
 *
 * post-conditions:
 * o  newly allocated and initalised entry is returned
 *
 * error-conditions:
 * o  if allocation fails, no operation is carried out, NULL is returned
 * --------------------------------------------------------------------------
 */
 
gll_string_repo_entry_t new_repo_entry (gll_string_t str, gll_hash_t key) {
  gll_string_repo_entry_t new_entry;
  
  /* allocate new entry */
  new_entry = malloc(sizeof(gll_string_repo_entry_s));
  
  /* initialise new entry */
  if (new_entry != NULL) {
    new_entry->str = str;
    new_entry->key = key;
    new_entry->next = NULL;
  } /* end if */
  
  return new_entry;
} /* end new_repo_entry */

/* END OF FILE */