/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-token.h
 *
 * Public interface for GLL unique string module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_UNIQUE_STRING_H
#define GLL_UNIQUE_STRING_H

#include "m2-common.h"


/* --------------------------------------------------------------------------
 * Dynamic string length limit
 * ----------------------------------------------------------------------- */

#define GLL_STRING_SIZE_LIMIT 240


/* --------------------------------------------------------------------------
 * opaque type gll_string_t
 * --------------------------------------------------------------------------
 * Opaque pointer type representing a dynamic string.
 * ----------------------------------------------------------------------- */

typedef struct gll_string_struct_t *gll_string_t;


/* --------------------------------------------------------------------------
 * type gll_string_status_t
 * --------------------------------------------------------------------------
 * Status codes for operations on type gll_string_t.
 * ----------------------------------------------------------------------- */

typedef enum {
  GLL_STRING_STATUS_SUCCESS,
  GLL_STRING_STATUS_NOT_INITIALIZED,
  GLL_STRING_STATUS_ALREADY_INITIALIZED,
  GLL_STRING_STATUS_INVALID_REFERENCE,
  GLL_STRING_STATUS_INVALID_INDICES,
  GLL_STRING_STATUS_ALLOCATION_FAILED,
} gll_string_status_t;


/* --------------------------------------------------------------------------
 * procedure gll_init_string_repository(size, status)
 * --------------------------------------------------------------------------
 * Allocates and initialises global string repository.  Parameter size
 * determines the number of buckets of the repository's internal hash table.
 * If size is zero, value GLL_STRING_REPO_DEFAULT_BUCKET_COUNT is used.
 *
 * pre-conditions:
 * o  global repository must be uninitialised upon entry
 * o  parameter size may be zero upon entry
 * o  parameter status may be NULL upon entry
 *
 * post-conditions:
 * o  global string repository is allocated and intialised.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if repository has already been initialised upon entry,
 *    no operation is carried out and GLL_STRING_STATUS_ALREADY_INITIALIZED
 *    is passed back in status unless status is NULL
 * o  if repository allocation failed, GLL_STRING_STATUS_ALLOCATION_FAILED
 *    is passed back in status unless status is NULL
 * --------------------------------------------------------------------------
 */

void gll_init_string_repository
  (uint_t size, gll_string_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_get_string(str, status)
 * --------------------------------------------------------------------------
 * Returns a unique string object for str which must be a pointer to a NUL
 * terminated character string.  
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  if a string object for str is present in the internal repository,
 *    that string object is retrieved, retained and returned.
 * o  if no string object for str is present in the repository,
 *    a new string object with a copy of str is created, stored and returned.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if str is NULL upon entry, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_INVALID_REFERENCE is
 *    passed back in status unless status is NULL
 * o  if string object allocation failed, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_ALLOCATION_FAILED is
 *    passed back in status unless status is NULL
 * --------------------------------------------------------------------------
 */

gll_string_t gll_get_string (char *str, gll_string_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_get_string_for_slice(str, offset, length, status)
 * --------------------------------------------------------------------------
 * Returns a unique string object for a given slice of str.  Parameter str
 * must be a pointer to a NUL terminated character string.  The position of
 * the first character of the slice is given by parameter offset and the
 * length of the slice is given by parameter length.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 * o  slice must be within range of str (NOT GUARDED),
 * o  slice must not contain any characters with code points < 32 or > 126 
 * 
 *
 * post-conditions:
 * o  if a string object for the slice is present in the internal repository,
 *    that string object is retrieved, retained and returned.
 * o  if no string object for the slice is present in the repository, a new
 *    string object with a copy of the slice is created, stored and returned.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if precondition #1 is not met, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_INVALID_REFERENCE is
 *    passed back in status, unless status is NULL
 * o  if precondition #3 is not met, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_INVALID_INDICES is
 *    passed back in status, unless status is NULL
 * o  if string object allocation failed, no operation is carried out,
 *    NULL is returned and GLL_STRING_STATUS_ALLOCATION_FAILED is
 *    passed back in status unless status is NULL
 * --------------------------------------------------------------------------
 */

gll_string_t gll_get_string_for_slice
  (char *str, uint_t offset, uint_t length, gll_string_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_get_string_for_concatenation(str, append_str, status)
 * --------------------------------------------------------------------------
 * Returns a unique string object for the character string resulting from
 * concatenation of str and append_str.  Parameters str and append_str must
 * be pointers to NUL terminated character strings.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 * o  parameter append_str must not be NULL upon entry
 *
 * post-conditions:
 * o  if a string object for the resulting concatenation string is present in
 *    the internal repository, that string object is retrieved, retained and
 *    returned.
 * o  if no string object for the resulting concatenation is present in the
 *    repository, a new string object with a copy of the concatenation is
 *    created, stored and returned.
 * o  GLL_STRING_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if any of str or append_str is NULL upon entry, no operation is
 *    carried out, NULL is returned and GLL_STRING_STATUS_INVALID_REFERENCE
 *    is passed back in status, unless NULL
 * o  if no dynamic string object could be allocated, NULL is returned,
 *    and GLL_STRING_STATUS_ALLOCATION_FAILED is passed back in status,
 *    unless NULL
 * --------------------------------------------------------------------------
 */

gll_string_t gll_get_string_for_concatenation
  (char *str, char *append_str, gll_string_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_string_length(str)
 * --------------------------------------------------------------------------
 * Returns the length of the character string of str.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  the length of str's character string is returned
 *
 * error-conditions:
 * o  if str is NULL upon entry, zero is returned
 * --------------------------------------------------------------------------
 */

inline uint_t gll_string_length (gll_string_t str);


/* --------------------------------------------------------------------------
 * function gll_string_char_ptr(str)
 * --------------------------------------------------------------------------
 * Returns an immutable pointer to the character string of str.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  an immutable pointer to str's character string is returned
 *
 * error-conditions:
 * o  if str is NULL upon entry, NULL is returned
 * --------------------------------------------------------------------------
 */

inline const char *gll_string_char_ptr (gll_string_t str);


/* --------------------------------------------------------------------------
 * function gll_unique_string_count()
 * --------------------------------------------------------------------------
 * Returns the number of unique strings stored in the string repository.
 *
 * pre-conditions:
 * o  none
 *
 * post-conditions:
 * o  none
 *
 * error-conditions:
 * o  none
 * --------------------------------------------------------------------------
 */

inline uint_t gll_unique_string_count (void);


/* --------------------------------------------------------------------------
 * function gll_string_retain(str)
 * --------------------------------------------------------------------------
 * Prevents str from deallocation.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  str's reference count is incremented.
 *
 * error-conditions:
 * o  if str is not NULL upon entry, no operation is carried out
 * --------------------------------------------------------------------------
 */

inline void gll_string_retain (gll_string_t str);


/* --------------------------------------------------------------------------
 * function gll_string_release(str)
 * --------------------------------------------------------------------------
 * Cancels an outstanding retain, or deallocates str if there are no
 * outstanding retains.
 *
 * pre-conditions:
 * o  parameter str must not be NULL upon entry
 *
 * post-conditions:
 * o  if str's reference count is zero upon entry, str is deallocated
 * o  if str's reference count is not zero upon entry, it is decremented
 *
 * error-conditions:
 * o  if str is not NULL upon entry, no operation is carried out
 * --------------------------------------------------------------------------
 */

void gll_string_release (gll_string_t str);


#endif /* GLL_UNIQUE_STRING_H */

/* END OF FILE */