/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-fileutils.h
 *
 * Public interface for filesystem utility functions.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_FILEUTILS_H
#define GLL_FILEUTILS_H

#include <stddef.h>
#include <stdbool.h>


/* --------------------------------------------------------------------------
 * Invalid file size indicator
 * ----------------------------------------------------------------------- */

#define INVALID_FILE_SIZE -1


/* --------------------------------------------------------------------------
 * function is_valid_filename(filename)
 * --------------------------------------------------------------------------
 * Returns true if filename is a valid filename, otherwise false.
 * --------------------------------------------------------------------------
 */

bool is_valid_filename (const char *filename);


/* --------------------------------------------------------------------------
 * function is_valid_pathname(pathname)
 * --------------------------------------------------------------------------
 * Returns true if pathname is a valid pathname, otherwise false.
 * --------------------------------------------------------------------------
 */

bool is_valid_pathname (const char *pathname);


/* --------------------------------------------------------------------------
 * function file_exists(filename)
 * --------------------------------------------------------------------------
 * Returns true if the file indicated by filename exists, otherwise false.
 * --------------------------------------------------------------------------
 */

bool file_exists (const char *filename);


/* --------------------------------------------------------------------------
 * function filesize(filename)
 * --------------------------------------------------------------------------
 * Returns the file size of a file.  Returns INVALID_FILE_SIZE on failure.
 * --------------------------------------------------------------------------
 */

int filesize (const char *filename);


/* --------------------------------------------------------------------------
 * function macro get_cwd(cwd, size)
 * --------------------------------------------------------------------------
 * Obtains current directory path in cwd and returns a pointer to it.
 * --------------------------------------------------------------------------
 */

char *get_cwd(char *cwd, size_t size);


#endif /* GLL_FILEUTILS_H */

/* END OF FILE */