/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-lexer.h
 *
 * Public interface for GLL lexer module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_LEXER_H
#define GLL_LEXER_H

#include "gll-token.h"
#include "gll-unique-string.h"


/* --------------------------------------------------------------------------
 * Lexical limits
 * ----------------------------------------------------------------------- */

#define GLL_MAX_IDENT_LENGTH 32


/* --------------------------------------------------------------------------
 * opaque type gll_lexer_t
 * --------------------------------------------------------------------------
 * Opaque pointer type representing a Modula-2 lexer object.
 * ----------------------------------------------------------------------- */

typedef struct gll_lexer_struct_t *gll_lexer_t;


/* --------------------------------------------------------------------------
 * type gll_lexer_status_t
 * --------------------------------------------------------------------------
 * Status codes for operations on type gll_lexer_t.
 * ----------------------------------------------------------------------- */

typedef enum {
  GLL_LEXER_STATUS_SUCCESS,
  GLL_LEXER_STATUS_INVALID_REFERENCE,
  GLL_LEXER_STATUS_ALLOCATION_FAILED,
  /* TO DO : add status codes for all error scenarios */
} gll_lexer_status_t;


/* --------------------------------------------------------------------------
 * procedure gll_new_lexer(lexer, filename, status)
 * --------------------------------------------------------------------------
 * Allocates a new object of type gll_lexer_t, opens an input file and
 * associates the opened file with the newly created lexer object.
 *
 * pre-conditions:
 * o  parameter lexer must be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  pointer to newly allocated and opened lexer is passed back in lexer
 * o  GLL_LEXER_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if lexer is not NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * o  if the file represented by filename cannot be found
 *    status GLL_LEXER_STATUS_FILE_NOT_FOUND is returned
 * o  if the file represented by filename cannot be accessed
 *    status GLL_LEXER_STATUS_FILE_ACCESS_DENIED is returned
 * o  if no infile object could be allocated
 *    status GLL_LEXER_STATUS_ALLOCATION_FAILED is returned
 * ----------------------------------------------------------------------- */

void gll_new_lexer
  (gll_lexer_t *lexer, gll_string_t filename, gll_lexer_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_read_sym(lexer)
 * --------------------------------------------------------------------------
 * Reads the lookahead symbol from the source file associated with lexer and
 * consumes it, thus advancing the current reading position, then returns
 * the symbol's token.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are updated
 * o  file status is set to GLL_LEXER_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * ----------------------------------------------------------------------- */

gll_token_t gll_read_sym (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * function gll_next_sym(lexer)
 * --------------------------------------------------------------------------
 * Reads the lookahead symbol from the source file associated with lexer but
 * does not consume it, thus not advancing the current reading position,
 * then returns the symbol's token.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are NOT updated
 * o  file status is set to GLL_LEXER_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * ----------------------------------------------------------------------- */

gll_token_t gll_next_sym (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * function gll_lexer_filename(lexer)
 * --------------------------------------------------------------------------
 * Returns the filename associated with lexer.
 * ----------------------------------------------------------------------- */

gll_string_t gll_lexer_filename (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * function gll_lexer_status(lexer)
 * --------------------------------------------------------------------------
 * Returns the status of the last operation on lexer.
 * ----------------------------------------------------------------------- */

gll_lexer_status_t gll_lexer_status (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * function gll_lexer_current_lexeme(lexer)
 * --------------------------------------------------------------------------
 * Returns the lexeme of the most recently consumed symbol.
 * ----------------------------------------------------------------------- */

gll_string_t gll_lexer_current_lexeme (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * function gll_lexer_current_line(lexer)
 * --------------------------------------------------------------------------
 * Returns the line counter of the most recently consumed symbol.
 * ----------------------------------------------------------------------- */

uint_t gll_lexer_current_line (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * function gll_lexer_current_column(lexer)
 * --------------------------------------------------------------------------
 * Returns the column counter of the most recently consumed symbol.
 * ----------------------------------------------------------------------- */

uint_t gll_lexer_current_column (gll_lexer_t lexer);


/* --------------------------------------------------------------------------
 * procedure gll_release_lexer(lexer, status)
 * --------------------------------------------------------------------------
 * Closes the file associated with lexer, deallocates its file object,
 * deallocates the lexer object and returns NULL in lexer.
 *
 * pre-conditions:
 * o  parameter lexer must not be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  file object is deallocated
 * o  NULL is passed back in lexer
 * o  GLL_LEXER_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if lexer is NULL upon entry, no operation is carried out
 *    and status GLL_LEXER_STATUS_INVALID_REFERENCE is returned
 * ----------------------------------------------------------------------- */

void gll_release_lexer (gll_lexer_t *lexer, gll_lexer_status_t *status);


#endif /* GLL_LEXER_H */

/* END OF FILE */