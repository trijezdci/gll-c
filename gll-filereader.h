/* Grammar Tool for LL(1) Grammars
 * Copyright (c) 2015 Benjamin Kowarsch
 *
 * @synopsis
 *
 * GLL is a tool to verify LL(1) grammars in an EBNF format. It generates
 * FIRST and FOLLOW sets and reports LL(1) violations.
 *
 * @file
 *
 * gll-filereader.h
 *
 * Public interface for GLL input file reader module.
 *
 * @license
 *
 * GLL is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License Version 2 (GPL2) as
 * published by the Free Software Foundation.
 *
 * GLL is distributed in the hope that it will be useful,  but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GLL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLL_FILEREADER_H
#define GLL_FILEREADER_H

#include "gll-unique-string.h"

#include <stdbool.h>


/* --------------------------------------------------------------------------
 * File size, line and column counter limits
 * --------------------------------------------------------------------------
 */

#define GLL_INFILE_MAX_SIZE 260000

#define GLL_INFILE_MAX_LINES 64000

#define GLL_INFILE_MAX_COLUMNS 250


/* --------------------------------------------------------------------------
 * opaque type gll_infile_t
 * --------------------------------------------------------------------------
 * Opaque pointer type representing a GLL input file.
 * --------------------------------------------------------------------------
 */

typedef struct gll_infile_struct_t *gll_infile_t;


/* --------------------------------------------------------------------------
 * type gll_infile_status_t
 * --------------------------------------------------------------------------
 * Status codes for operations on type gll_infile_t.
 * --------------------------------------------------------------------------
 */

typedef enum {
  GLL_INFILE_STATUS_SUCCESS,
  GLL_INFILE_STATUS_INVALID_REFERENCE,
  GLL_INFILE_STATUS_FILE_NOT_FOUND,
  GLL_INFILE_STATUS_FILE_ACCESS_DENIED,
  GLL_INFILE_STATUS_ALLOCATION_FAILED,
  GLL_INFILE_STATUS_FILE_EMPTY,
  GLL_INFILE_STATUS_ATTEMPT_TO_READ_PAST_EOF,
  GLL_INFILE_STATUS_IO_SUBSYSTEM_ERROR
} gll_infile_status_t;


/* --------------------------------------------------------------------------
 * procedure gll_open_infile(infile, filename, status)
 * --------------------------------------------------------------------------
 * Allocates a new object of type gll_infile_t, opens an input file and
 * associates the opened file with the newly created infile object.
 *
 * pre-conditions:
 * o  parameter file must be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  pointer to newly allocated and opened file is passed back in infile
 * o  line and column counters of the newly allocated infile are set to 1
 * o  GLL_INFILE_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if infile is not NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * o  if the file represented by filename cannot be found
 *    status GLL_INFILE_STATUS_FILE_NOT_FOUND is returned
 * o  if the file represented by filename cannot be accessed
 *    status GLL_INFILE_STATUS_FILE_ACCESS_DENIED is returned
 * o  if no infile object could be allocated
 *    status GLL_INFILE_STATUS_ALLOCATION_FAILED is returned
 * --------------------------------------------------------------------------
 */

gll_infile_t gll_open_infile
  (gll_string_t filename, gll_infile_status_t *status);


/* --------------------------------------------------------------------------
 * function gll_read_char(infile)
 * --------------------------------------------------------------------------
 * Reads the lookahead character from infile, advancing the current reading
 * position, updating line and column counter and returns its character code.
 * Returns EOF if the lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

int gll_read_char (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_mark_lexeme(infile)
 * --------------------------------------------------------------------------
 * Marks the current lookahead character as the start of a lexeme.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  position of lookahead character is stored internally
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

void gll_mark_lexeme (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_read_marked_lexeme(infile)
 * --------------------------------------------------------------------------
 * Returns a string object with the character sequence starting with the
 * character that has been marked using procedure gll_mark_lexeme() and
 * ending with the last consumed character.  Returns NULL if no marker
 * has been set or if the marked character has not been consumed.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  marked position is cleared
 * o  dynamic string with lexeme is returned
 *
 * error-conditions:
 * o  if infile is NULL upon entry,
 *    no operation is carried out and NULL is returned
 * --------------------------------------------------------------------------
 */

gll_string_t gll_read_marked_lexeme (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_consume_char(infile)
 * --------------------------------------------------------------------------
 * Consumes the current lookahead character, advancing the current reading
 * position, updating line and column counter and returns the character code
 * of the new lookahead character that follows the consumed character.
 * Returns EOF if the lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

int gll_consume_char (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_next_char(infile)
 * --------------------------------------------------------------------------
 * Reads the lookahead character from infile without advancing the current
 * reading position and returns its character code.  Returns EOF if the
 * lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of lookahead character or EOF is returned
 * o  current reading position and line and column counters are NOT updated
 * o  file status is set to GLL_INFILE_STATUS_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

int gll_next_char (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_la2_char(infile)
 * --------------------------------------------------------------------------
 * Reads the second lookahead character from infile without advancing the
 * current reading position and returns its character code.  Returns EOF
 * if the second lookahead character lies beyond the end of infile.
 *
 * pre-conditions:
 * o  parameter infile must not be NULL upon entry
 *
 * post-conditions:
 * o  character code of second lookahead character or EOF is returned
 * o  current reading position and line and column counters are NOT updated
 * o  file status is set to GLL_INFILE_STATUC_SUCCESS
 *
 * error-conditions:
 * o  if infile is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * --------------------------------------------------------------------------
 */

int gll_la2_char (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_infile_filename(infile)
 * --------------------------------------------------------------------------
 * Returns the filename associated with infile.
 * --------------------------------------------------------------------------
 */

gll_string_t gll_infile_filename (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_infile_status(infile)
 * --------------------------------------------------------------------------
 * Returns the status of the last operation on file.
 * --------------------------------------------------------------------------
 */

gll_infile_status_t gll_infile_status (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_infile_eof(infile)
 * --------------------------------------------------------------------------
 * Returns true if the current reading position of infile lies beyond the end
 * of the associated file, returns false otherwise.
 * --------------------------------------------------------------------------
 */

bool gll_infile_eof (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_infile_current_line(infile)
 * --------------------------------------------------------------------------
 * Returns the current line counter of infile.
 * --------------------------------------------------------------------------
 */

uint_t gll_infile_current_line (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * function gll_infile_current_column(infile)
 * --------------------------------------------------------------------------
 * Returns the current column counter of infile.
 * --------------------------------------------------------------------------
 */

uint_t gll_infile_current_column (gll_infile_t infile);


/* --------------------------------------------------------------------------
 * procedure gll_close_infile(file, status)
 * --------------------------------------------------------------------------
 * Closes the file associated with handle file, deallocates its file object
 * and returns NULL in handle file.
 *
 * pre-conditions:
 * o  parameter file must not be NULL upon entry
 * o  parameter status may be NULL
 *
 * post-conditions:
 * o  file object is deallocated
 * o  NULL is passed back in file
 * o  GLL_INFILE_STATUS_SUCCESS is passed back in status, unless NULL
 *
 * error-conditions:
 * o  if file is NULL upon entry, no operation is carried out
 *    and status GLL_INFILE_STATUS_INVALID_REFERENCE is returned
 * -------------------------------------------------------------------------- 
 */

void gll_close_infile (gll_infile_t *infptr, gll_infile_status_t *status);


#endif /* GLL_FILEREADER_H */

/* END OF FILE */